import React, { useContext } from 'react';
import propTypes from 'prop-types';
import './Floor.css';
import jiraffeImg from '../../assets/jiraffe-32x32.png';
import AuthContext from '../../providers/AuthContext';

const FloorMatrix = ({ floorMatrix, editMode, editMatrix }) => {
  const { user } = useContext(AuthContext);

  const renderCell = (cell, rowIndex, columnIndex) => {
    const classNames = {
      0: 'empty-space',
      '-1': 'available',
      '-2': 'forbidden',
      '-3': 'ooo-space',
    };

    let className = 'box ' + (cell > 0 && cell !== user?.id ? 'taken' : classNames[cell + '']);

    if (editMode) {
      className += ' vox ';
    }

    return (
      <div className={className} key={columnIndex}>
        <div className='ellipsis nav nav-pills'>
          {editMode ? (
            <div className='nav-item dropdown'>
              <span
                className='nav-link'
                data-toggle='dropdown'
                role='button'
                aria-haspopup='true'
                aria-expanded='false'
              ></span>
              {user && user.id === cell ? <img src={jiraffeImg} alt='jiraffe' /> : null}
              <div className='dropdown-menu'>
                {Object.keys(classNames).map((key) => {
                  return (
                    <button
                      className='dropdown-item'
                      key={key}
                      value={key}
                      onClick={(e) => editMatrix(rowIndex, columnIndex, e.target.value)}
                    >
                      {classNames[key]}
                    </button>
                  );
                })}
              </div>
            </div>
          ) : (
            <span>
              {/* {cell === -1 ? 'Available' : cell === -2 ? 'Forbidden' : cell > 0 ? 'Taken' : ''} */}
              {/* {cell} */}
              {user && user.id === cell ? <img src={jiraffeImg} alt='jiraffe' /> : null}
            </span>
          )}
        </div>
      </div>
    );
  };

  const renderRow = (row, rowIndex) => {
    return (
      <div className='row-custom' key={rowIndex}>
        {row.map((cell, columnIndex) => renderCell(cell, rowIndex, columnIndex))}
      </div>
    );
  };

  return (
    <div className='matrix'>{floorMatrix?.map((row, rowIndex) => renderRow(row, rowIndex))}</div>
  );
};

FloorMatrix.propTypes = {
  floorMatrix: propTypes.array,
};

export default FloorMatrix;
