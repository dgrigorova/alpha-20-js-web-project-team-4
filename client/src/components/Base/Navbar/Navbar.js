import React, { useContext, useState } from 'react';
import { BASE_URL } from '../../../common/constants';
import AuthContext, { getToken } from '../../../providers/AuthContext';
import { NavLink, withRouter } from 'react-router-dom';
import AppError from '../AppError/AppError';

const Navbar = ({ history }) => {
  const { user, isLoggedIn, setLoginState } = useContext(AuthContext);
  const [error, setError] = useState(null);

  const logout = (e) => {
    e.preventDefault();
    localStorage.removeItem('token');

    setLoginState({
      isLoggedIn: false,
      user: null,
    });
    history.push('/home');

    fetch(`${BASE_URL}/session`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${getToken()}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
      })
      .catch((error) => setError(error.message));
  };

  if (error) {
    return (
      <div className='jumbotron'>
        <AppError message={error} />
      </div>
    );
  }

  return (
    <nav className='navbar navbar-expand-lg navbar-dark bg-primary'>
      <NavLink className='navbar-brand' to='/home'>
        Jiraffe
      </NavLink>
      <button
        className='navbar-toggler'
        type='button'
        data-toggle='collapse'
        data-target='#navbarColor01'
        aria-controls='navbarColor01'
        aria-expanded='false'
        aria-label='Toggle navigation'
      >
        <span className='navbar-toggler-icon'></span>
      </button>

      <div className='collapse navbar-collapse' id='navbarColor01'>
        <ul className='navbar-nav mr-auto'>
          <li className='nav-item'>
            <NavLink className='nav-link' to='/home'>
              Home
            </NavLink>
          </li>
          {isLoggedIn ? (
            <>
              <li className='nav-item'>
                <NavLink className='nav-link' to='/account'>
                  My Account
                </NavLink>
              </li>
              {user.role === 'Admin' ? (
                <li className='nav-item'>
                  <NavLink className='nav-link' to='/dashboard'>
                    Dashboard
                  </NavLink>
                </li>
              ) : null}
              <li className='nav-item'>
                <NavLink className='nav-link' to='/home' onClick={logout}>
                  Log out
                </NavLink>
              </li>
            </>
          ) : (
            <>
              <li className='nav-item'>
                <NavLink className='nav-link' to='/login'>
                  Sign In
                </NavLink>
              </li>
            </>
          )}
        </ul>
        <form className='form-inline my-2 my-lg-0'>
          <input className='form-control mr-sm-2' type='text' placeholder='Search'></input>
          <button className='btn btn-secondary my-2 my-sm-0' type='submit'>
            Search
          </button>
        </form>
      </div>
    </nav>
  );
};

export default withRouter(Navbar);
