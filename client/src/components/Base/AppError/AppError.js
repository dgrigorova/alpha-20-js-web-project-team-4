import React from 'react';
import PropTypes from 'prop-types';

const AppError = ({ message, setError }) => {
  return (
    <div className='AppError'>
      <p className='error-message'>{message}</p>
      {/* <button type='info' className='login btn btn-info' onClick={() => setError()}>
        Try again
      </button> */}
    </div>
  );
};

AppError.propTypes = {
  message: PropTypes.string.isRequired,
};

export default AppError;
