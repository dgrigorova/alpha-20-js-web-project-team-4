import React from 'react';
import PropTypes from 'prop-types';
import './Loader.css';

const Loader = ({ children }) => {
  return (
    <div className="loader">
      {children}
    </div>
  )
};

Loader.propTypes = {
  children: PropTypes.element.isRequired,
};

export default Loader;
