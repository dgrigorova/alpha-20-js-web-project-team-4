import React, { useEffect, useState } from 'react';
import propTypes from 'prop-types';
import './EmployeesTable.css';
import { BASE_URL } from '../../../../common/constants';
import { getToken } from '../../../../providers/AuthContext';
import AppError from '../../../Base/AppError/AppError';
import { Row, Col, Card, Table } from 'react-bootstrap';

const EmployeesTable = ({ countryId }) => {
  const [employees, setEmployees] = useState([]);
  const [error, setError] = useState(null);
  const [showTable, setShowTable] = useState(false);

  useEffect(() => {
    fetch(`${BASE_URL}/users/countries/${countryId}`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (result) {
          setEmployees(result);
        } else {
          throw new Error(result.message);
        }
      })
      .catch((error) => setError(error));
  }, [countryId]);

  if (error) {
    return <AppError message={error} />;
  }

  const renderTable = () => {
    let index = 0;
    const table = employees.map((employee) => {
      return (
        <tr key={employee?.id}>
          <th scope='row'>{++index}</th>
          <td>{employee?.fullName}</td>
          <td>{employee?.project?.name}</td>
          <td>{showStatus(employee?.currentStatus)}</td>
          <td>{showStatus(employee?.nextPeriodStatus)}</td>
        </tr>
      );
    });
    return table;
  };

  const showStatus = (status) => {
    if (status === 1) {
      return 'Unassigned';
    }
    if (status === 2) {
      return 'Office';
    }
    if (status === 3) {
      return 'Home';
    }
    if (status === 4) {
      return 'Vacation';
    }
  };

  return (
    <>
      <Row>
        <Col>
          <Card>
            <Card.Header className='table-header' onClick={() => setShowTable(!showTable)}>
              <Card.Title as='h5'>Employees Schedule</Card.Title>
            </Card.Header>
            {!showTable ? (
              <Card.Body>
                <Table striped responsive>
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Employee Name</th>
                      <th>Project Name</th>
                      <th>Current Week Location</th>
                      <th>Next Week Location</th>
                    </tr>
                  </thead>
                  <tbody>{renderTable()}</tbody>
                </Table>
              </Card.Body>
            ) : null}
          </Card>
        </Col>
      </Row>
    </>
  );
};

EmployeesTable.propTypes = {
  countryId: propTypes.number,
};
export default EmployeesTable;
