import './Workspace.css';
import React, { useState, useEffect } from 'react';
import propTypes from 'prop-types';
import { getToken } from '../../../providers/AuthContext';
import { BASE_URL } from '../../../common/constants';
import AppError from '../../Base/AppError/AppError';
import { withRouter } from 'react-router-dom';
import Loader from '../../Base/Loader/Loader';
import jiraffeImg from '../../../assets/jiraffe-32x32.png';
import FloorMatrix from '../../Floor/Floor';

const Workspace = ({ floorId, match, history, location }) => {
  const id = match.params['id'];
  const [editMode, setEditMode] = useState(location.pathname.includes('edit'));
  const [workspace, setWorkspace] = useState([]);
  const [loading, setLoading] = useState(false);
  const [showManageDesks, setShowManageDesks] = useState(true);
  const [showEmployeesPlan, setShowEmployeesPlan] = useState(true);


  const [error, setError] = useState(null);
  const [workspaceDTO, setWorkspaceDTO] = useState({
    name: workspace?.name,
    country: workspace?.country?.id,
    matrix: [[]],
  });

  const updateWorkspaceProp = (prop, value) => {
    setWorkspace({
      ...workspace,
      [prop]: value,
    });

    setWorkspaceDTO({
      ...workspaceDTO,
      name: workspace?.name,
      country: workspace?.country?.id,
      [prop]: value,
    });
  };

  const updateCell = (rowIndex, columnIndex, status) => {
    const newMatrix = [...workspace?.matrix];

    newMatrix[rowIndex][columnIndex] = +status;

    updateWorkspaceProp('matrix', [...newMatrix]);
  };

  useEffect(() => {
    setLoading(true);

    fetch(`${BASE_URL}/planning/floors/${floorId || id}`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (result) {
          setWorkspace(result);
        } else {
          throw new Error(result.message);
        }
      })
      .catch((error) => setError(error))
      .finally(() => setLoading(false));
  }, [floorId, id]);

  if (error) {
    return <AppError message={error} />;
  }

  if (loading) {
    return (
      <Loader>
        <h1>Loading...</h1>
      </Loader>
    );
  }

  const renderLegend = () => {
    return (
      <>
        <p>Legend:</p>
        <div className='l'>
          <div className='floor-legend'>
            <div className='legend-box'>
              <div className='legend'>
                <img src={jiraffeImg} alt='jiraffe' />
              </div>
              <div className='legend available'></div>
              <div className='legend forbidden'></div>
              <div className='legend taken'></div>
            </div>
            <div className='legend-box'>
              <div className='legend legend-text'>Your Desk</div>
              <div className='legend legend-text'>Available</div>
              <div className='legend legend-text'>Forbidden</div>
              <div className='legend legend-text'>Occupied</div>
            </div>
          </div>
        </div>
        <hr></hr>
      </>
    );
  };

  const editWorkspace = () => {
    fetch(`${BASE_URL}/planning/floors/${id}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${getToken()}`,
      },

      body: JSON.stringify(workspaceDTO),
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }

        setWorkspace({ ...result });
        history.push('/dashboard');
      })
      .catch((error) => setError(error.message));
  };

  const getAvailableDesks = () => {
    fetch(`${BASE_URL}/planning/floors/${floorId || id}/available-desks`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${getToken()}`,
      },

      body: JSON.stringify(workspaceDTO),
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }

        setWorkspace({ ...result });
        // history.push('/dashboard');
      })
      .catch((error) => setError(error.message));
  };

  const assignUsers = () => {
    fetch(`${BASE_URL}/planning/floors/${floorId || id}/next-period`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${getToken()}`,
      },

      body: JSON.stringify(workspaceDTO),
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }

        setWorkspace({ ...result });
      })
      .catch((error) => setError(error.message));
  };

  console.log(workspace);

  return (
    <>
      {editMode ?
        <>
          <div className='tabs'>
            <button type="button" className="btn btn-outline-primary manage-button" onClick={() => setShowManageDesks(!showManageDesks)}>{showManageDesks ? 'Hide Manage Desks' : 'Manage Desks'}</button>            <button type="button" className="btn btn-outline-primary manage-button" onClick={() => setShowEmployeesPlan(!showEmployeesPlan)}>{showEmployeesPlan ? 'Hide Employees Schedule' : 'Generate Employees Schedule'}</button>
          </div>
          <hr></hr>
        </>
        : null}
      {workspace?.country?.name ? <h4>Floor Plan for {workspace?.country?.name}</h4> : null}
      <br></br>
      { workspace?.country?.name ? renderLegend() : null}
      <div className='matrix-container'>
        {workspace?.matrix && editMode && showManageDesks ? (
          <>
            <p>Floor Plan</p>
            <FloorMatrix
              floorMatrix={[...workspace?.matrix]}
              editMode={editMode}
              editMatrix={(rowIndex, columnIndex, status) =>
                updateCell(rowIndex, columnIndex, status)
              }
            />
            <div className='tabs'>
            <button
            type='submit'
            className='btn btn-primary submit manage-button'
            onClick={() => {
              editWorkspace();
            }}
          >
            Submit Changes
          </button>
          <button
            type='submit'
            className='btn btn-primary submit manage-button'
            onClick={() => {
              getAvailableDesks();
            }}
          >
            Generate Available Desks
          </button>
          </div>
            <hr></hr>
          </>
        ) : null}
        {workspace?.matrixCurrentPeriod && showEmployeesPlan ? (
          <>
            <p>Current Week Plan</p>
            <FloorMatrix
              floorMatrix={[...workspace?.matrixCurrentPeriod]}
              editMode={editMode}
              editMatrix={(rowIndex, columnIndex, status) =>
                updateCell(rowIndex, columnIndex, status)
              }
            /> 
        <hr></hr>
          </>
        ) : null}
        {workspace?.matrixNextPeriod && showEmployeesPlan ? (
          <>
            <p>Next Week Plan</p>
            <FloorMatrix
              floorMatrix={[...workspace?.matrixNextPeriod]}
              editMode={editMode}
              editMatrix={(rowIndex, columnIndex, status) =>
                updateCell(rowIndex, columnIndex, status)
              }
            />
            {editMode ? 
            <button
            type='submit'
            className='btn btn-primary submit'
            onClick={() => {
              assignUsers();
            }}
          >
            Assign Users
          </button>
: null}
          </>
        ) : null}
      </div>
    </>
  );
};

Workspace.propTypes = {
  floorId: propTypes.string,
};

export default withRouter(Workspace);
