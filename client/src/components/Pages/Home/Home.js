import EmployeesTable from '../Tables/EmployeesTable/EmployeesTable';
import { getToken } from '../../../providers/AuthContext';
import { BASE_URL } from '../../../common/constants';
import AppError from '../../Base/AppError/AppError';
import React, { useEffect, useState } from 'react';
import Workspace from '../Workspace/Workspace';
import './Home.css';

const Home = () => {
  const [allFloors, setAllFloors] = useState([]);
  const [floorId, setFloorId] = useState('');
  const [error, setError] = useState(null);

  useEffect(() => {
    fetch(`${BASE_URL}/planning/floors`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (result) {
          setAllFloors(result);
        } else {
          throw new Error(result.message);
        }
      })
      .catch((error) => setError(error));
  }, []);

  if (error) {
    return <AppError message={error} />;
  }

  const floorsList = () => {
    return (
      <div className='form-group'>
        <label htmlFor='exampleSelect1'>Select floor:</label>
        <select
          className='form-control'
          id='exampleSelect1'
          value={floorId}
          onChange={(e) => setFloorId(e.target.value)}
        >
          {allFloors.map((floor) => {
            return (
              <option key={floor?.id} value={floor?.id}>
                {floor?.country?.name}
              </option>
            );
          })}
        </select>
      </div>
    );
  };

  const getCountryId = (floor) => {
    const currfloor = allFloors.filter((f) => f.id === +floor);
    const countryId = currfloor[0]?.country?.id;
    return countryId;
  };

  return (
    <>
      <div className='hero-container jumbotron'>
        <h1 className='home-text'>Welcome to Jiraffe</h1>
        <p className='home-add-text'>Your online workspace.</p><br></br>
        {floorsList()}
        {floorId ? <Workspace floorId={floorId} /> : null}
        {floorId ? <EmployeesTable countryId={getCountryId(floorId)} /> : null}
      </div>
    </>
  );
};

export default Home;
