import React from 'react';
import './NotFound.css';

const NotFound = (props) => {
    return (
        <div className="jumbotron">
            <h1 className="display-3">404</h1>
            <hr className="my-4"></hr>
            <h3>SORRY, PAGE NOT FOUND</h3>
            <button type="button" className="btn btn-primary" onClick={() => props.history.push(`/home`)}>Go to homepage</button>
        </div>
    );
};

export default NotFound;
