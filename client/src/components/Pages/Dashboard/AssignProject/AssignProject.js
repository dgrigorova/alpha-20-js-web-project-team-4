import React, { useEffect, useState } from 'react';
import './AssignProject.css';
import { BASE_URL } from '../../../../common/constants';
import { getToken } from '../../../../providers/AuthContext';
import { Link } from 'react-router-dom';
import AppError from '../../../Base/AppError/AppError';

const AssignProject = ({ history, match }) => {
    const userId = match.params['id'];
    const [user, setUser] = useState([]);
    const [project, setProject] = useState('');
    const [allProjects, setAllProjects] = useState([]);
    const [error, setError] = useState(null);
   
    useEffect(() => {
        fetch(`${BASE_URL}/users/${userId}`, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${getToken()}`,
            }
        })
            .then(response => response.json())
            .then(result => {
                if (result.error) {
                    throw new Error(result.message);
                }
                setUser(result);
            })
            .catch(error => setError(error.message));
    }, [userId]);

    useEffect(() => {
        fetch(`${BASE_URL}/projects`, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${getToken()}`,
            }
        })
            .then(response => response.json())
            .then(result => {
                if (result.error) {
                    throw new Error(result.message);
                }
                setAllProjects(result);
            })
            .catch(error => setError(error.message));
    }, []);

    const assignProject = (e) => {
        e.preventDefault();
        
        fetch(`${BASE_URL}/users/${user.id}/projects/${project}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${getToken()}`,
            },
        })
            .then(response => response.json())
            .then(result => {
                if (result.error) {
                    throw new Error(result.message);
                };

                history.push('/dashboard');
            })
            .catch(error => setError(error.message));
    }

    if (error) {
        return <AppError message={error} />;
    }

    const ProjectsForm = () => {
        return (
          <div className='form-group assign-form'>
            <h5 htmlFor='exampleSelect1'>Assign {user?.fullName} to:</h5><br></br>
            <select
              className='form-control'
              id='exampleSelect1'
              value={project}
              onChange={(e) => setProject(e.target.value)}
            >
              {allProjects.map((project) => {
                return (
                  <option key={project?.id} value={project?.id}>
                    {project?.name}
                  </option>
                );
              })}
            </select>
            <button type="submit" className="btn btn-primary submit" onClick={assignProject}>Submit</button>
           <p> <Link to="/dashboard">Cancel</Link></p>
          </div>
        );
      };

    return (
        <div className='jumbotron'>
        {ProjectsForm()}
        </div>
    )
}

export default AssignProject;