import React, { useEffect, useState } from 'react';
import './CreateFloor.css';
import { getToken } from '../../../../../providers/AuthContext';
import { BASE_URL } from '../../../../../common/constants';
import AppError from '../../../../Base/AppError/AppError';
import { Link } from 'react-router-dom';

const CreateFloor = ({ history }) => {
  const [allCountries, setAllCountries] = useState([]);
  const [error, setError] = useState(null);
  const [floor, setFloor] = useState({
    name: '',
    country: 0,
    rows: 0,
    columns: 0,
  });

  const updateFloorProp = (prop, value) => {
    setFloor({
      ...floor,
      [prop]: value,
    });
  };

  useEffect(() => {
    fetch(`${BASE_URL}/countries`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${getToken()}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
        setAllCountries(result);
      })
      .catch((error) => setError(error.message));
  }, []);

  const createFloor = (e) => {
    e.preventDefault();
    console.log(floor);

    fetch(`${BASE_URL}/planning/floors`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${getToken()}`,
      },
      body: JSON.stringify({
        ...floor,
        country: +floor.country,
        rows: +floor.rows,
        columns: +floor.columns,
      }),
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }

        history.push('/dashboard');
      })
      .catch((error) => setError(error.message));
  };

  if (error) {
    return <AppError message={error} setError={() => setError(null)} />;
  }

  const formElements = {
    name: {
      labelContent: 'Name',
      type: 'text',
      className: 'form-control',
      id: 'name',
      propName: 'name',
    },
    country: {
      labelContent: 'Select Country',
      type: 'text',
      className: 'form-control',
      id: 'country',
      propName: 'country',
    },
    rows: {
      propName: 'rows',
      labelContent: 'Rows',
      type: 'text',
      className: 'form-control',
      id: 'rows',
    },
    columns: {
      labelContent: 'Columns',
      type: 'text',
      className: 'form-control',
      id: 'columns',
      propName: 'columns',
    },
  };

  const renderFormElements = () => {
    return (
      <form>
        <fieldset>
          <div className='form-group jumbotron'>
          <h4>Create Floor</h4><br></br>    
            {Object.keys(formElements).map((key, index) => {
              return (
                <div key={index} className='create-floor'>
                  <label htmlFor='exampleInputEmail1'>{formElements[key].labelContent}</label>
                  {formElements[key].propName === 'country' ? (
                    <select
                      className='form-control'
                      id='exampleSelect1'
                      value={floor.country}
                      onChange={(e) => updateFloorProp(formElements[key].propName, e.target.value)}
                    >
                      {allCountries.map((country) => {
                        return (
                          <option key={country?.id} value={country?.id}>
                            {country?.name}
                          </option>
                        );
                      })}
                    </select>
                  ) : (
                    <input
                      type={formElements[key].type}
                      className={formElements[key].className}
                      id={formElements[key].id}
                      onChange={(e) => updateFloorProp(formElements[key].propName, e.target.value)}
                    ></input>
                  )}
                </div>
              );
            })}
            <button
              type='submit'
              className='btn btn-primary submit'
              onClick={(e) => {
                createFloor(e);
              }}
            >
              Create floor
            </button>
            <p> <Link to="/dashboard">Cancel</Link></p>
          </div>
        </fieldset>
      </form>
    );
  };

  // const floorsList = () => {
  //   return (
  //     <div className='form-group'>
  //       <label htmlFor='exampleSelect1'>Select floor:</label>
  //       <select
  //         className='form-control'
  //         id='exampleSelect1'
  //         value={floor}
  //         onChange={(e) => setFloor(e.target.value)}
  //       >
  //         {allFloors.map((floor) => {
  //           return (
  //             <option key={floor?.id} value={floor?.id}>
  //               {floor?.country?.name}
  //             </option>
  //           );
  //         })}
  //       </select>
  //     </div>
  //   );
  // };

  // const getCountryId = (floor) => {
  //   const currfloor = allFloors.filter((f) => f.id === +floor);
  //   const countryId = currfloor[0]?.country?.id;
  //   return countryId;
  // };

  // <div>
  //   {/* {floorsList()} */}
  //   {/* {floor ? <EmployeesTable countryId={getCountryId(floor)} /> : null}
  //       <Workspace floorId={floor} />
  //       <button
  //         type='button'
  //         className='btn btn-secondary add-cancel'
  //         onClick={() => setAddFloor(!addFloor)}
  //       >
  //         {addFloor ? 'Cancel' : 'Add Floor'}
  //       </button>
  //       <AddFloor add={addFloor} /> */}
  // </div>;

  return <>{renderFormElements()}</>;
};

export default CreateFloor;
