import React, { useEffect, useState } from 'react';
import { getToken } from '../../../../providers/AuthContext';
import { Row, Col, Card, Table } from 'react-bootstrap';
import { BASE_URL } from '../../../../common/constants';
import AppError from '../../../Base/AppError/AppError';
import { withRouter } from 'react-router-dom';

const ManageFloor = ({ history }) => {
  const [allFloors, setAllFloors] = useState([]);
  const [error, setError] = useState(null);
  const [fold, setFold] = useState(false);

  useEffect(() => {
    fetch(`${BASE_URL}/planning/floors`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (result) {
          setAllFloors(result);
        } else {
          throw new Error(result.message);
        }
      })
      .catch((error) => setError(error));
  }, []);

  if (error) {
    return <AppError message={error} />;
  }

  const renderAllFloors = () => {
    let index = 0;
    return (
      <>
        <Row>
          <Col>
            <Card>
              <Card.Header className='table-header' onClick={() => setFold(!fold)}>
                <Card.Title as='h5'>Manage Floors</Card.Title>
              </Card.Header>
              {!fold ? (
                <Card.Body>
                  <Table striped hover responsive>
                    <thead>
                      <tr>
                        <th>
                          <i
                            className='fas fa-plus'
                            onClick={() => history.push(`/floors/create`)}
                          ></i>
                          <span> Add</span>
                        </th>
                        <th>Floor</th>
                        <th>Country</th>
                        <th>Number of desks</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      {allFloors.map((floor) => {
                        return (
                          <tr key={floor?.id}>
                            <th scope='row'>{++index}</th>
                            <td>{floor?.name}</td>
                            <td>{floor?.country?.name}</td>
                            <td>{floor?.additionalData?.desks}</td>
                            <td>
                              <i
                                className='far fa-edit'
                                value={floor?.id}
                                onClick={(е) => history.push(`/workspace/edit/${floor?.id}`)}
                              ></i>
                            </td>
                          </tr>
                        );
                      })}
                    </tbody>
                  </Table>
                </Card.Body>
              ) : null}
            </Card>
          </Col>
        </Row>
      </>
    );
  };

  return renderAllFloors();
};

export default withRouter(ManageFloor);
