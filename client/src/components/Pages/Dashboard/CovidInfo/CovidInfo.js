import React, { useEffect, useState } from 'react';
import './CovidInfo.css'
import { BASE_URL } from '../../../../common/constants';
import { getToken } from '../../../../providers/AuthContext';
import AppError from '../../../Base/AppError/AppError';
import { Row, Col, Card, Table } from 'react-bootstrap';

const CovidInfo = () => {
    const [error, setError] = useState(null);
    const [allCountries, setAllCountries] = useState([]);
    const [countryName, setCountryName] = useState('Bulgaria');
    const [info, setInfo] = useState([]);
    const [renderTable, setRenderTable] = useState(false);

    useEffect(() => {
        fetch(`${BASE_URL}/countries`, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${getToken()}`,
            }
        })
            .then(response => response.json())
            .then(result => {
                if (result.error) {
                    throw new Error(result.message);
                }
                setAllCountries(result);
            })
            .catch(error => setError(error.message));
    }, []);

    const getCovidInfo = () => {

        fetch(`${BASE_URL}/countries/${countryName}`, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${getToken()}`,
            },
        })
            .then(response => response.json())
            .then(result => {
                if (result.error) {
                    throw new Error(result.message);
                }
                setInfo(result);
            })
            .catch(error => setError(error.message));
    }

    if (error) {
        return <AppError message={error} />;
    }

    const getInfoForm = () => {
        return (
            <div className='form-group assign-form covid'>
                <p>Get current COVID information</p>
                <label htmlFor='exampleSelect1'>Select Country</label>
                <select
                    className='form-control'
                    id='exampleSelect1'
                    value={countryName}
                    onChange={e => setCountryName(e.target.value)}
                >
                    {allCountries.map((country) => {
                        return (
                            <option key={country?.id} value={country?.name}>
                                {country?.name}
                            </option>
                        );
                    })}
                </select>
                <button type="submit" className="btn btn-primary submit" onClick={() => {
                    getCovidInfo();
                    setRenderTable(true);
                    }}>Get info</button>
            </div>
        );
    };

    const renderCovidInfo = () => {
        return (
            <div className='covid-info'>
                <Row>
                    <Col>
                        <Card>
                            <Card.Header>
                                <Card.Title as='h5'>Covid Info for {info?.country}</Card.Title>
                            </Card.Header>
                            <Card.Body>
                                <Table striped responsive>
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Cases</th>
                                            <th>Today Cases</th>
                                            <th>Deaths</th>
                                            <th>Today Deaths</th>
                                            <th>Active</th>
                                            <th>Recovered</th>
                                            <th>Critical</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr key={info?.id}>
                                            <th scope='row'></th>
                                            <td>{info.cases}</td>
                                            <td>{info.todayCases}</td>
                                            <td>{info.deaths}</td>
                                            <td>{info.todayDeaths}</td>
                                            <td>{info.active}</td>
                                            <td>{info.recovered}</td>
                                            <td>{info.critical}</td>
                                        </tr>
                                    </tbody>
                                </Table>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </div>
        );
    };

    return (
        <>
            { getInfoForm()}
            {renderTable?  renderCovidInfo() : null}
        </>
    )
}

export default CovidInfo;