import React, { useEffect, useState } from 'react';
import { BASE_URL } from '../../../../../common/constants';
import { getToken } from '../../../../../providers/AuthContext';
import AppError from '../../../../Base/AppError/AppError';
import { Link } from 'react-router-dom';

const EditProject = ({ history, match }) => {

    const projectId = match.params['id'];
    const [project, setProject] = useState({
        name: '',
        country: '',
    });
    const [allCountries, setAllCountries] = useState([]);
    const [error, setError] = useState(null);

    useEffect(() => {
        fetch(`${BASE_URL}/countries`, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${getToken()}`,
            }
        })
            .then(response => response.json())
            .then(result => {
                if (result.error) {
                    throw new Error(result.message);
                }
                setAllCountries(result);
            })
            .catch(error => setError(error.message));
    }, []);

    useEffect(() => {
        fetch(`${BASE_URL}/projects/${projectId}`, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${getToken()}`,
            }
        })
            .then(response => response.json())
            .then(result => {
                if (result.error) {
                    throw new Error(result.message);
                }
                setProject(result);
            })
            .catch(error => setError(error.message));
    }, [projectId]);

    const update = (e) => {
        e.preventDefault();
        
        fetch(`${BASE_URL}/projects/${projectId}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${getToken()}`,
            },
            body: JSON.stringify(project),
        })
            .then(response => response.json())
            .then(result => {
                if (result.error) {
                    throw new Error(result.message);
                };

                history.push('/dashboard');
            })
            .catch(error => setError(error.message));
    }

    const updateProject = (prop, value) => setProject({
        ...project,
        [prop]: value
    });

    const editProjectForm = () => {
        return (
            
          <div className='form-group assign-form'>
              <h4>Edit Project</h4><br></br>              
              <div className="form-group">
                  <input type="text" className="form-control" id="project-name" value={project.name} onChange={e => updateProject('name', e.target.value)} placeholder="Project Name"></input>
                </div>
            <label htmlFor='exampleSelect1'>Select Country</label>
            <select
              className='form-control'
              id='exampleSelect1'
              value={project.country}
              onChange={e => updateProject('country', e.target.value)}
            >
              {allCountries.map((country) => {
                return (
                  <option key={country?.id} value={country?.id}>
                    {country?.name}
                  </option>
                );
              })}
            </select>
            <button type="submit" className="btn btn-primary submit" onClick={update}>Submit</button>
           <p> <Link to="/dashboard">Cancel</Link></p>
          </div>
        );
      };

    if (error) {
        return <AppError message={error} />;
    }

    return (
        <div className='jumbotron'>
            {editProjectForm()}
        </div>
    )
}

export default EditProject;