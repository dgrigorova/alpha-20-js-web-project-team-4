import React, { useEffect, useState } from 'react';
import './ManageProjects.css';
import { Row, Col, Card, Table } from 'react-bootstrap';
import { BASE_URL } from '../../../../common/constants';
import { getToken } from '../../../../providers/AuthContext';
import AppError from '../../../Base/AppError/AppError';
import { withRouter } from 'react-router-dom';

const ManageProject = ({ history }) => {
  const [allProjects, setAllProjects] = useState([]);
  const [allUsers, setAllUsers] = useState([]);
  const [error, setError] = useState(null);
  const [fold, setFold] = useState(false);
  const [foldTwo, setFoldTwo] = useState(false);
  useEffect(() => {
    fetch(`${BASE_URL}/projects`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (result) {
          setAllProjects(result);
        } else {
          throw new Error(result.message);
        }
      })
      .catch((error) => setError(error));
  }, []);

  useEffect(() => {
    fetch(`${BASE_URL}/users`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (result) {
          setAllUsers(result);
        } else {
          throw new Error(result.message);
        }
      })
      .catch((error) => setError(error));
  }, []);

  const deleteProject = (id) => {
    fetch(`${BASE_URL}/projects/${id}`, {
      method: 'DELETE',
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
        setAllProjects(allProjects.filter(project => project.id !== id));

      })
      .catch((error) => setError(error.message));
  }

  if (error) {
    return <AppError message={error} />;
  }

  const renderAllFloors = () => {
    let index = 0;
    return (
      <>
        <Row>
          <Col>
            <Card>
              <Card.Header className='table-header' onClick={() => setFold(!fold)}>
                <Card.Title as="h5">Manage Projects</Card.Title>
              </Card.Header>
              {!fold ?
                <Card.Body>
                  <Table striped hover responsive>
                    <thead>
                      <tr>
                        <th><i className="fas fa-plus" onClick={() => history.push(`/projects/create`)}></i><span> Add</span></th>
                        <th>Project Name</th>
                        <th>Country</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      {allProjects.map(project => {
                        return (
                          <tr key={project?.id}>
                            <th scope="row">{++index}</th>
                            <td>{project?.name}</td>
                            <td>{project?.country?.name}</td>
                            <td><i className="far fa-edit" onClick={() => history.push(`/projects/${project?.id}`)}></i>
                              <span></span>
                              <i className="far fa-trash-alt" onClick={() => deleteProject(project?.id)}></i>
                            </td>
                          </tr>
                        )
                      })}
                    </tbody>
                  </Table>
                </Card.Body>
                : null}
            </Card>
          </Col>
        </Row>
      </>
    );
  }

  const renderAllProjects = () => {
    let index = 0;
    return (
      <>
        <Row>
          <Col>
            <Card>
              <Card.Header className='table-header' onClick={() => setFoldTwo(!foldTwo)}>
                <Card.Title as="h5">Assign Project</Card.Title>
              </Card.Header>
              {!foldTwo ?
                <Card.Body>
                  <Table striped hover responsive>
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Employee</th>
                        <th>Project Name</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      {allUsers.map(user => {
                        return (
                          <tr key={user?.id}>
                            <th scope="row">{++index}</th>
                            <td>{user?.fullName}</td>
                            <td>{user?.project?.name ? user?.project?.name : 'Unassigned'}</td>
                            <td><i className="far fa-edit" onClick={() => history.push(`/projects/${user?.id}/assign`)}></i></td>
                          </tr>
                        )
                      })}
                    </tbody>
                  </Table>
                </Card.Body>
                : null}
            </Card>
          </Col>
        </Row>
      </>
    );
  }

  return (
    <>
      {renderAllFloors()}
      {renderAllProjects()}
    </>
  )
}

export default withRouter(ManageProject);
