import React, { useEffect, useState } from 'react';
import './Dashboard.css';
import ManageProject from './ManageProjects/ManageProjects';
import { getToken } from '../../../providers/AuthContext';
import { BASE_URL } from '../../../common/constants';
import ManageFloor from './ManageFloor/ManageFloor';
import AppError from '../../Base/AppError/AppError';
import CovidInfo from './CovidInfo/CovidInfo';

const Dashboard = () => {
  const [allFloors, setAllFloors] = useState([]);
  const [error, setError] = useState(null);
  const [showManageFloor, setShowManageFloor] = useState(false);
  const [showManafeProjects, setShowManageProjects] = useState(false);
  const [showCovidInfo, setShowCovidInfo] = useState(false);
  // const [floor, setFloor] = useState('');

  useEffect(() => {
    fetch(`${BASE_URL}/planning/floors`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (result) {
          setAllFloors(result);
        } else {
          throw new Error(result.message);
        }
      })
      .catch((error) => setError(error));
  }, []);

  if (error) {
    return <AppError message={error} />;
  }

  return (
    <>
      <div className='tabs'>
        <button type="button" className="btn btn-outline-primary manage-button" onClick={() => setShowCovidInfo(!showCovidInfo)}>{showCovidInfo ? 'Hide Covid Info' : 'Get Covid Info'}</button>
        <button type="button" className="btn btn-outline-primary manage-button" onClick={() => setShowManageFloor(!showManageFloor)}>{showManageFloor ? 'Hide Floors' : 'Manage Floors'}</button>
        <button type="button" className="btn btn-outline-primary" onClick={() => setShowManageProjects(!showManafeProjects)}>{showManafeProjects ? 'Hide Projects' : 'Manage Projects'}</button>
      </div>
      <hr></hr>
      {showCovidInfo ? <CovidInfo /> : null}
      {showManageFloor ? <ManageFloor /> : null}
      {showManafeProjects ? <ManageProject /> : null}
    </>
  );
};

export default Dashboard;
