import React, { useContext, useState } from 'react';
import './Login.css';
import propTypes from 'prop-types';
import { Link } from 'react-router-dom';
import AuthContext, { extractUser } from '../../../providers/AuthContext';
import { BASE_URL } from '../../../common/constants';
import AppError from '../../Base/AppError/AppError';

const LogIn = ({ history }) => {

    const { setLoginState } = useContext(AuthContext);
    const [user, setUserObject] = useState({
        email: {
            value: '',
            touched: false,
            valid: true,
        },
        password: {
            value: '',
            touched: false,
            valid: true,
        },
    });

    const [error, setError] = useState(null);

    const updateUser = (prop, value) => setUserObject({
        ...user,
        [prop]: {
            value,
            touched: true,
            valid: userValidators[prop].reduce((isValid, validatorFn) => isValid && (typeof validatorFn(value) !== 'string'), true),
        }
    });

    const validateForm = () => !Object
        .keys(user)
        .reduce((isValid, prop) => isValid && user[prop].valid && user[prop].touched, true);

    const userValidators = {
        email: [
            value => /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$/.test(value) || `Please provide a valid email address.`,
          ],
        password: [
            value => value?.length >= 4 || `Password should be at least 4 letters.`,
            value => value?.length <= 10 || `Password should be no more than 10 letters.`,
        ],
    };

    const getValidationErrors = (prop) => {
        return userValidators[prop]
            .map(validatorFn => validatorFn(user[prop].value))
            .filter(value => typeof value === 'string');
    };

    const renderValidationError = prop => user[prop].touched && !user[prop].valid
        ? getValidationErrors(prop).map((error, index) => <p className="error" key={index}>{error}</p>)
        : null;


    const getClassNames = (prop) => {
        let classes = '';
        if (user[prop].touched) {
            classes += 'form-control touched '
        }
        if (user[prop].valid) {
            classes += 'form-control valid ';
        } else {
            classes += 'form-control invalid is-invalid';
        }

        return classes;
    };

    const login = (e) => {
        e.preventDefault();

        fetch(`${BASE_URL}/session`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: user.email.value,
                password: user.password.value,
            }),
        })
            .then(response => response.json())
            .then(result => {
                if (result.error) {
                    throw new Error(result.message)
                }

                localStorage.setItem('token', result.token);

                setLoginState({
                    isLoggedIn: !!extractUser(result.token),
                    user: extractUser(result.token),
                });
                history.push('/account');
            })
            .catch(error => setError(error.message));
    }

    if (error) {
        return (
            <div className="jumbotron">
                <AppError message={error} />
                <button className="btn btn-primary" onClick={() => setError(null)}>Try again</button>
            </div>
        );
    }

    return (
        <form className='login'>
            <fieldset>
                <legend>Log in</legend>
                <div className="form-group">
                    <label htmlFor="exampleInputEmail1">Email address</label>
                    <input type="text" className={getClassNames('email')} id="email" value={user.email.value} onChange={(e) => updateUser('email', e.target.value)} placeholder="Email address"></input>
                    {renderValidationError('email')}
                </div>
                <div className="form-group">
                    <label htmlFor="exampleInputPassword1">Password</label>
                    <input type="password" className={getClassNames('password')} id="password" value={user.password.value} onChange={e => updateUser('password', e.target.value)} placeholder="Password"></input>
                    {renderValidationError('password')}
                </div>
                <button type="submit" className="login btn btn-primary" disabled={validateForm()} onClick={(e) => login(e)}>Log in</button>
                <hr></hr>
                <p>Don't have an account? <Link to="/register">Register here</Link></p>
            </fieldset>
        </form>
    );
}

LogIn.propTypes = {
    history: propTypes.object.isRequired
};

export default LogIn;