import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { BASE_URL } from '../../../common/constants';
import AppError from '../../Base/AppError/AppError';

const Register = ({ history }) => {

  const [user, setUserObject] = useState({
    username: {
      value: '',
      touched: false,
      valid: true,
    },
    fullName: {
      value: '',
      touched: false,
      valid: true,
    },
    country: {
      value: '',
      touched: false,
      valid: true,
    },
    email: {
      value: '',
      touched: false,
      valid: true,
    },
    password: {
      value: '',
      touched: false,
      valid: true,
    },
    confirmPassword: {
      value: '',
      touched: false,
      valid: true,
    },
  });

  const [error, setError] = useState(null);
  const [countries, setCountries] = useState([]);

  useEffect(() => {

    fetch(`${BASE_URL}/countries`)
      .then((response) => response.json())
      .then((result) => {
        if (Array.isArray(result)) {
          setCountries(result);

        } else {
          throw new Error(result.message);
        }
      })
      .catch((error) => setError(error))
  }, []);

  const getCountries = countries.map(c => {
    return (
      <option key={c.id} value={c.id}>{c.name}</option>
    )
  })

  const updateUser = (prop, value) => setUserObject({
    ...user,
    [prop]: {
      value,
      touched: true,
      valid: userValidators[prop]?.reduce((isValid, validatorFn) => isValid && (typeof validatorFn(value) !== 'string'), true),
    }
  });

  const validateForm = () => !Object
    .keys(user)
    .reduce((isValid, prop) => isValid && user[prop].valid && user[prop].touched, true);

  const userValidators = {
    username: [
      value => value?.length >= 4 || `Username should be at least 4 letters.`,
      value => value?.length <= 15 || `Username should be no more than 15 letters.`,
    ],
    fullName: [
      value => value?.length >= 2 || `Country name should be at least 2 letters.`,
      value => value?.length <= 50 || `Country name should be no more than 50 letters.`,
    ],
    country: [
      value => value?.length >= 2 || `Name should be at least 2 letters.`,
      value => value?.length <= 10 || `Name should be no more than 10 letters.`,
    ],
    email: [
      value => /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$/.test(value) || `Please provide a valid email address.`,
    ],
    password: [
      value => value?.length >= 4 || `Password should be at least 4 letters.`,
      value => value?.length <= 10 || `Password should be no more than 10 letters.`,
    ],
    confirmPassword: [
      value => value === user.password.value || `Passwords don't match`,
    ],
  };

  const getValidationErrors = (prop) => {
    return userValidators[prop]
      .map(validatorFn => validatorFn(user[prop].value))
      .filter(value => typeof value === 'string');
  };

  const renderValidationError = prop => user[prop].touched && !user[prop].valid
    ? getValidationErrors(prop).map((error, index) => <p className="error" key={index}>{error}</p>)
    : null;


  const getClassNames = (prop) => {
    let classes = '';
    if (user[prop].touched) {
      classes += 'form-control touched '
    }
    if (user[prop].valid) {
      classes += 'form-control valid ';
    } else {
      classes += 'form-control invalid is-invalid';
    }

    return classes;
  };

  const register = (e) => {
    e.preventDefault();

    fetch(`${BASE_URL}/users`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        username: user.username.value,
        fullName: user.fullName.value,
        country: user.country.value,
        email: user.email.value,
        password: user.password.value,
      }),
    })
      .then(response => response.json())
      .then(result => {
        if (result.error) {
          throw new Error(result.message)
        }

        history.push('/login');
      })
      .catch(error => setError(error.message));
  }

  if (error) {
    return (
      <div className="jumbotron">
        <AppError message={error} />
      </div>
    );
  }


  return (
    <form className='login'>
      <fieldset>
        <legend>Create a new account</legend>
        <div className="form-group">
          <input type="text" className={getClassNames('username')} id="username" value={user.username.value} onChange={e => updateUser('username', e.target.value)} placeholder="Username"></input>
          {renderValidationError('username')}
        </div>
        <div className="form-group">
          <input type="text" className={getClassNames('fullName')} id="full-name" value={user.fullName.value} onChange={e => updateUser('fullName', e.target.value)} placeholder="Full name"></input>
          {renderValidationError('fullName')}
        </div>
        <div className="form-group">
          <label htmlFor="exampleSelect1">Choose your country</label>
          <select id="exampleSelect1 country" className={getClassNames('country')} value={user.country.value} onChange={e => updateUser('country', e.target.value)}>
            {getCountries}
          </select>
        </div>

        <div className="form-group">
          <input type="email" className={getClassNames('email')} id="email" value={user.email.value} onChange={e => updateUser('email', e.target.value)} placeholder="Email address"></input>
          {renderValidationError('email')}
        </div>
        <div className="form-group">
          <input type="password" className={getClassNames('password')} id="password" value={user.password.value} onChange={e => updateUser('password', e.target.value)} placeholder="Password"></input>
          {renderValidationError('password')}
        </div>
        <div className="form-group">
          <input type="password" className={getClassNames('confirmPassword')} id="confirm-password" value={user.confirmPassword.value} onChange={e => updateUser('confirmPassword', e.target.value)} placeholder="Confirm Password"></input>
          {renderValidationError('confirmPassword')}
        </div>
        <button type="submit" className="login btn btn-primary" disabled={validateForm()} onClick={register}>Sign up</button>
        <hr></hr>
        <p>Have an account? <Link to="/login">Log in</Link></p>

      </fieldset>
    </form>
  );
}

export default Register;