import AuthContext, { getToken } from '../../../providers/AuthContext';
import React, { useContext, useEffect, useState } from 'react';
import { Row, Col, Card, Table } from 'react-bootstrap';
import { BASE_URL } from '../../../common/constants';
import AppError from '../../Base/AppError/AppError';
import Vacation from './Vacation/Vacation';
import './Profile.css';

const Profile = () => {
  const { user } = useContext(AuthContext);
  const [error, setError] = useState(null);
  const [userDetails, setUserDetails] = useState([]);

  useEffect(() => {
    fetch(`${BASE_URL}/users/${user.id}`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
        setUserDetails(result);
      })
      .catch((error) => setError(error.message));
  }, [user.id]);

  if (error) {
    return <AppError message={error} />;
  }

  const showStatus = (status) => {
    if (status === 1) {
      return 'Unassigned';
    }
    if (status === 2) {
      return 'Office';
    }
    if (status === 3) {
      return 'Home';
    }
    if (status === 4) {
      return 'Vacation';
    }
  };

  const renderSchedule = () => {
    return (
      <div className='employee-schedule'>
        <Row>
          <Col>
            <Card>
              <Card.Header>
                <Card.Title as='h5'>Personal Schedule</Card.Title>
              </Card.Header>
              <Card.Body>
                <Table striped responsive>
                  <thead>
                    <tr>
                      <th></th>
                      <th>Current Week</th>
                      <th>Next Week</th>
                      <th>Project Assigned</th>
                      <th>Country</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr key={userDetails?.id}>
                      <th scope='row'></th>
                      <td>{showStatus(userDetails?.currentStatus)}</td>
                      <td>{showStatus(userDetails?.nextPeriodStatus)}</td>
                      <td>{userDetails?.project?.name}</td>
                      <td>{userDetails?.country?.name}</td>
                    </tr>
                  </tbody>
                </Table>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </div>
    );
  };

  return (
    <div className='jumbotron'>
      <h4>My Account</h4><br></br>
      {renderSchedule()}
      <hr></hr>
      <Vacation />
    </div>
  );
};

export default Profile;
