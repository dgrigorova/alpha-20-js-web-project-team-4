import React, { useState } from 'react';
import './AddVacation.css';
import moment from 'moment';
import { getToken } from '../../../../../providers/AuthContext';
import { BASE_URL } from '../../../../../common/constants';
import AppError from '../../../../Base/AppError/AppError';
import { Link } from 'react-router-dom';

const AddVacation = ({ history }) => {
  const [error, setError] = useState(null);
  const vacation = {
    startDate: moment().format(`YYYY-M-D`),
    endDate: moment().format(`YYYY-M-D`),
  };

  const updateVacationProp = (prop, value) => {
    vacation[prop] = value;
  };

  const createVacation = (e) => {
    e.preventDefault();

    fetch(`${BASE_URL}/users/vacation`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${getToken()}`,
      },
      body: JSON.stringify(vacation),
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
        history.push('/account');
      })
      .catch((error) => setError(error.message));
  };

  if (error) {
    return (
      <div className='jumbotron'>
        <AppError message={error} />
        <button className='btn btn-primary' onClick={() => setError(null)}>
          Go back
        </button>
      </div>
    )

  }

  const addVacationForm = () => {
    return (
      <form>
        <fieldset>
          <div className='form-group add-vacation'>
            <h4>Add Vacation</h4><br></br>
            <label htmlFor='exampleInputEmail1'>Start Date:</label>
            <input
              type='date'
              className='form-control'
              id='start-date'
              onChange={(e) => updateVacationProp('startDate', e.target.value)}
            ></input>
            <label htmlFor='exampleInputEmail1'>End Date:</label>
            <input
              type='date'
              className='form-control'
              id='end-date'
              onChange={(e) => updateVacationProp('endDate', e.target.value)}
            ></input>
            <button type='submit' className='btn btn-primary submit' onClick={createVacation}>
              Submit
            </button>
            <p> <Link to="/account">Cancel</Link></p>
          </div>
        </fieldset>
      </form>
    );
  };

  return (
    <div className='jumbotron'>
      {addVacationForm()}
    </div>
  )

};

export default AddVacation;
