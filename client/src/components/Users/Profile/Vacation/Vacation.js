import React, { useContext, useEffect, useState } from 'react';
import './Vacation.css';
import AuthContext, { getToken } from '../../../../providers/AuthContext';
import { BASE_URL } from '../../../../common/constants';
import AppError from '../../../Base/AppError/AppError';
import { Row, Col, Card, Table } from 'react-bootstrap';
import moment from 'moment';
import { withRouter } from 'react-router-dom';

const Vacation = ({ history }) => {

    const { user } = useContext(AuthContext);
    const [vacations, setVacations] = useState([]);
    const [updatedVacations, setUpdatedVacations] = useState(vacations);
    const [error, setError] = useState(null);

    useEffect(() => {
        fetch(`${BASE_URL}/users/${user.id}/vacation`, {
            headers: {
                Authorization: `Bearer ${getToken()}`,
            },
        })
            .then((response) => response.json())
            .then((result) => {
                if (result.error) {
                    throw new Error(result.message);
                }
                setVacations(result);
            })
            .catch((error) => setError(error.message));
    }, [user.id, updatedVacations]);

    if (error) {
        return <AppError message={error} />;
    }

    const deleteVacation = (vacationId) => {
        fetch(`${BASE_URL}/users/vacation/${vacationId}`, {
            method: 'DELETE',
            headers: {
                Authorization: `Bearer ${getToken()}`,
            },
        })
            .then((response) => response.json())
            .then((result) => {
                if (result.error) {
                    throw new Error(result.message);
                }
                setUpdatedVacations(updatedVacations.filter(vacation => vacation.id !== vacationId));
            })
            .catch((error) => setError(error.message));
    }

    const renderTable = () => {
        let index = 0;
        const table = vacations.map(vacation => {

            return (
                <tr key={vacation?.id}>
                    <th scope="row">{++index}</th>
                    <td>{moment(vacation.startDate).format('LL')}</td>
                    <td>{moment(vacation.endDate).format('LL')}</td>
                    <td><i className="far fa-trash-alt" onClick={() => deleteVacation(vacation?.id)}></i></td>
                </tr>
            )
        })
        return table;
    }

    const renderVacations = () => {
        return (
            <div className="all-vacations">
                <Row>
                    <Col>
                        <Card>
                            <Card.Header>
                                <Card.Title as="h5">Manage Vacations</Card.Title>
                            </Card.Header>
                            <Card.Body>
                                <Table striped hover responsive>
                                    <thead>
                                        <tr>
                                            <th><i className="fas fa-plus" onClick={() => history.push(`/account/vacations`)}></i><span> Add</span></th>
                                            <th>Start Date</th>
                                            <th>End Date</th>
                                            <th>Cancel</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {renderTable()}
                                    </tbody>
                                </Table>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </div>
        )
    }

    return (
        renderVacations()
    )
}
export default withRouter(Vacation);