import 'bootswatch/dist/flatly/bootstrap.min.css';
import React, { useState } from 'react';
import './App.css';
import CreateProject from './components/Pages/Dashboard/ManageProjects/CreateProject/CreateProject';
import EditProject from './components/Pages/Dashboard/ManageProjects/EditProject/EditProject';
import CreateFloor from './components/Pages/Dashboard/ManageFloor/CreateFloor/CreateFloor';
import AddVacation from './components/Users/Profile/Vacation/AddVacation/AddVacation';
import AssignProject from './components/Pages/Dashboard/AssignProject/AssignProject';
import AuthContext, { extractUser, getToken } from './providers/AuthContext';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import Workspace from './components/Pages/Workspace/Workspace';
import Dashboard from './components/Pages/Dashboard/Dashboard';
import NotFound from './components/Pages/NotFound/NotFound';
import Register from './components/Users/Register/Register';
import Profile from './components/Users/Profile/Profile';
import Navbar from './components/Base/Navbar/Navbar';
import GuardedRoute from './providers/GuardedRoute';
import LogIn from './components/Users/Login/Login';
import Home from './components/Pages/Home/Home';

function App() {
  const [authValue, setAuthValue] = useState({
    isLoggedIn: !!extractUser(getToken()),
    user: extractUser(getToken()),
  });

  return (
    <BrowserRouter>
      <AuthContext.Provider value={{ ...authValue, setLoginState: setAuthValue }}>
        <Navbar />
        <Switch>
          <Redirect path='/' exact to='/home' />
          <Route path='/home' component={Home} />
          <Route path='/login' component={LogIn} />
          <Route path='/register' component={Register} />
          <GuardedRoute
            path='/account/vacations'
            auth={authValue.isLoggedIn}
            exact
            component={AddVacation}
          />
          <GuardedRoute path='/account' auth={authValue.isLoggedIn} exact component={Profile} />
          <GuardedRoute
            path='/dashboard'
            auth={authValue.isLoggedIn && authValue.user.role === 'Admin'}
            exact
            component={Dashboard}
          />
          <GuardedRoute
            path='/projects/:id/assign'
            auth={authValue.isLoggedIn && authValue.user.role === 'Admin'}
            exact
            component={AssignProject}
          />
          <GuardedRoute
            path='/projects/create'
            auth={authValue.isLoggedIn && authValue.user.role === 'Admin'}
            exact
            component={CreateProject}
          />
          <GuardedRoute
            path='/projects/:id'
            auth={authValue.isLoggedIn && authValue.user.role === 'Admin'}
            exact
            component={EditProject}
          />
          <GuardedRoute
            path='/workspace/edit/:id'
            auth={authValue.isLoggedIn && authValue.user.role === 'Admin'}
            exact
            component={Workspace}
          />
          <GuardedRoute
            path='/floors/create'
            auth={authValue.isLoggedIn && authValue.user.role === 'Admin'}
            exact
            component={CreateFloor}
          />
          <Route path='*' component={NotFound} />
        </Switch>
      </AuthContext.Provider>
    </BrowserRouter>
  );
}

export default App;
