import { Country } from '../../country/model/country.entity';
import { Project } from '../../project/model/project.entity';
import { UserStatus } from '../../common/enums/user-status';
import { UserRole } from '../../common/enums/user-role';
import { Team } from '../../team/model/team.entity';
import { Vacation } from './vacation.entity';
import {
  PrimaryGeneratedColumn,
  Entity,
  Column,
  ManyToOne,
  OneToMany,
} from 'typeorm';

@Entity('user')
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ unique: true })
  username: string;

  @Column()
  fullName: string;

  @Column({ unique: true })
  email: string;

  @Column()
  password: string;

  @Column({
    type: 'enum',
    enum: UserRole,
    default: UserRole.Basic,
  })
  role: UserRole;

  @Column({
    type: 'enum',
    enum: UserStatus,
    default: UserStatus.Unassigned,
  })
  currentStatus: UserStatus;

  @Column({
    type: 'enum',
    enum: UserStatus,
    default: UserStatus.Unassigned,
  })
  nextPeriodStatus: UserStatus;

  @Column({
    nullable: true,
  })
  banEndDate: Date;

  @Column({ default: false })
  isDeleted: boolean;

  @ManyToOne(
    type => Project,
    project => project.assignedUsers,
    { nullable: true },
  )
  project: number;

  @ManyToOne(
    type => Country,
    country => country.employees,
  )
  country: number;

  @ManyToOne(
    type => Team,
    team => team.teamMembers,
  )
  team: number;

  @OneToMany(
    type => Vacation,
    vacation => vacation.user,
  )
  vacation: Vacation[];
}
