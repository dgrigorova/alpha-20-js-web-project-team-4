import { PrimaryGeneratedColumn, Entity, Column, ManyToOne } from 'typeorm';
import { User } from './user.entity';

@Entity('vacation')
export class Vacation {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    nullable: true,
  })
  startDate: Date;

  @Column({
    nullable: true,
  })
  endDate: Date;

  @ManyToOne(
    type => User,
    user => user.vacation,
  )
  user: number;

  @Column({ type: 'boolean', default: false })
  isDeleted: boolean;
}
