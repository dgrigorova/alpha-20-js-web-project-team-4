import { TransformService } from '../../common/services/transform.service';
import { getRepositoryToken, TypeOrmModule } from '@nestjs/typeorm';
import { FindService } from '../../common/services/find.service';
import { UsersController } from '../controller/users.controller';
import { Vacation } from './../model/vacation.entity';
import { Test, TestingModule } from '@nestjs/testing';
import { UsersService } from './users.service';
import { User } from '../model/user.entity';

describe('ServiceService', () => {
  let service: UsersService;

  const transformer = {
    toResponseUserDTO() {
      /* empty */
    },
  };

  const usersRepository = {
    find() {
      /* empty */
    },
    save() {
      /* empty */
    },
  };

  const vacationRepository = {
    find() {
      /* empty */
    },
    save() {
      /* empty */
    },
    update() {
      /* empty */
    },
  };

  const findService = {
    getVacationById() {
      /* empty */
    },
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UsersService,
        {
          provide: getRepositoryToken(User),
          useValue: usersRepository,
        },
        {
          provide: getRepositoryToken(Vacation),
          useValue: vacationRepository,
        },
        {
          provide: TransformService,
          useValue: transformer,
        },
        {
          provide: FindService,
          useValue: findService,
        },
      ],
      controllers: [UsersController],
    }).compile();

    service = module.get<UsersService>(UsersService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
