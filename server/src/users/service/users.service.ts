import { TransformService } from '../../common/services/transform.service';
import { ResponseVacationDTO } from '../dtos/response-vacation-dto';
import { FindService } from '../../common/services/find.service';
import { BadRequestException, Injectable } from '@nestjs/common';
import { CreateVacationDTO } from '../dtos/user-vacation-dto';
import { ResponseUserDTO } from '../dtos/response-user-dto';
import { UserStatus } from '../../common/enums/user-status';
import { ONE_DAY_IN_MS } from '../../constants/constants';
import { Cron, CronExpression } from '@nestjs/schedule';
import { CreateUserDTO } from '../dtos/create-user-dto';
import { Vacation } from '../model/vacation.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../model/user.entity';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private readonly usersRepository: Repository<User>,
    @InjectRepository(Vacation)
    private readonly vacationRepository: Repository<Vacation>,
    private readonly transformService: TransformService,
    private readonly findService: FindService,
  ) {}
  async getAll(): Promise<ResponseUserDTO[]> {
    const users = await this.usersRepository.find({
      relations: ['project'],
    });

    return users.map(user => this.transformService.toResponseUserDTO(user));
  }

  async create(userDTO: CreateUserDTO): Promise<ResponseUserDTO> {
    const user = new User();

    user.username = userDTO.username;
    user.fullName = userDTO.fullName;
    user.email = userDTO.email;
    user.password = await bcrypt.hash(userDTO.password, 10);
    user.country = userDTO.country;

    const userCreated = await this.usersRepository.save(user);

    return this.transformService.toResponseUserDTO(userCreated);
  }

  async getUserById(userId: number): Promise<ResponseUserDTO> {
    const user = await this.findService.getUserById(userId);

    return this.transformService.toResponseUserDTO(user);
  }

  async getUsersByCountry(countryId: number): Promise<ResponseUserDTO[]> {
    const users = await this.findService.getUsersByCountry(countryId);
    return users.map(user => this.transformService.toResponseUserDTO(user));
  }

  async assignProject(
    userId: number,
    projectId: number,
  ): Promise<{ message: string }> {
    const user = await this.findService.getUserById(userId);

    user.project = projectId;

    const updatedUser = await this.usersRepository.update(user.id, user);
    return {
      message: `Project with id ${projectId} has been assigned to user with id ${userId}.`,
    };
  }

  async getVacation(userId: number): Promise<ResponseVacationDTO[]> {
    const vacation = await this.vacationRepository.find({
      where: {
        user: userId,
        isDeleted: false,
      },
    });

    return vacation.map(v => this.transformService.toResponseVacationDTO(v));
  }

  async createVacation(
    vacationDto: CreateVacationDTO,
    userId: number,
  ): Promise<ResponseVacationDTO> {
    if (
      new Date(vacationDto.startDate).getTime() >=
      new Date(vacationDto.endDate).getTime()
    ) {
      throw new BadRequestException(`Please provide valid dates.`);
    }

    const vacation = new Vacation();
    vacation.startDate = vacationDto.startDate;
    vacation.endDate = vacationDto.endDate;
    vacation.user = userId;

    const savedVacation = await this.vacationRepository.save(vacation);

    return this.transformService.toResponseVacationDTO(savedVacation);
  }

  async editVacation(
    vacationId: number,
    vacationDTO: CreateVacationDTO,
  ): Promise<ResponseVacationDTO> {
    await this.vacationRepository.update(vacationId, vacationDTO);
    const updatedVacation = await this.findService.getVacationById(vacationId);

    return this.transformService.toResponseVacationDTO(updatedVacation);
  }

  async deleteVacation(
    vacationId: number,
    userId: number,
  ): Promise<{ message: string }> {
    const user = await this.findService.getUserById(userId);
    user.currentStatus = UserStatus.Unassigned;

    await this.usersRepository.save(user);

    const vacation = await this.findService.getVacationById(vacationId);

    vacation.isDeleted = true;
    await this.vacationRepository.update(vacation.id, vacation);

    return {
      message: `Vacation with id ${vacationId} is deleted.`,
    };
  }

  @Cron(CronExpression.EVERY_DAY_AT_10AM)
  async updateUserStatusBasedOnVacations() {
    const todayInMs = new Date().getTime();
    const today = new Date(todayInMs).toISOString();

    const yesterdayInMs = todayInMs - ONE_DAY_IN_MS;
    const yesterday = new Date(yesterdayInMs).toISOString();

    const expiredVacationsRecords: Array<{
      userId: number;
    }> = await this.vacationRepository
      .createQueryBuilder('vacation')
      .select('userId')
      .where('endDate < :today', {
        today,
      })
      .andWhere('endDate > :yesterday', { yesterday })
      .getRawMany();

    expiredVacationsRecords.map(
      async record =>
        await this.usersRepository.update(record.userId, {
          currentStatus: UserStatus.Home,
        }),
    );

    const tomorrowInMs = todayInMs + ONE_DAY_IN_MS;
    const tomorrow = new Date(tomorrowInMs).toISOString();

    const newVacationsRecords: Array<{
      userId: number;
    }> = await this.vacationRepository
      .createQueryBuilder('vacation')
      .select('userId')
      .where('startDate > :today', {
        today,
      })
      .andWhere('startDate < :tomorrow', { tomorrow })
      .getRawMany();

    newVacationsRecords.map(
      async record =>
        await this.usersRepository.update(record.userId, {
          currentStatus: UserStatus.Vacation,
        }),
    );
  }
}
