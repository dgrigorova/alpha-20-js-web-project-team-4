import { ResponseVacationDTO } from '../dtos/response-vacation-dto';
import { BlacklistGuard } from '../../auth/guards/blacklist.guard';
import { UserId } from '../../auth/decorators/user-id.decorator';
import { CreateVacationDTO } from '../dtos/user-vacation-dto';
import { ResponseUserDTO } from '../dtos/response-user-dto';
import { RolesGuard } from '../../auth/guards/roles.guard';
import { CreateUserDTO } from '../dtos/create-user-dto';
import { UsersService } from '../service/users.service';
import { UserRole } from '../../common/enums/user-role';
import {
  UseGuards,
  Controller,
  Get,
  Post,
  Body,
  ValidationPipe,
  Param,
  Put,
  Delete,
} from '@nestjs/common';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Get()
  @UseGuards(BlacklistGuard, new RolesGuard(UserRole.Admin))
  async allUsers(): Promise<ResponseUserDTO[]> {
    return await this.usersService.getAll();
  }

  @Post()
  async createUser(
    @Body(new ValidationPipe({ whitelist: true })) userDTO: CreateUserDTO,
  ): Promise<ResponseUserDTO> {
    return await this.usersService.create(userDTO);
  }

  @Get(':id')
  async getUserById(@Param('id') userId: string): Promise<ResponseUserDTO> {
    return this.usersService.getUserById(+userId);
  }

  @Get('countries/:countryId')
  async getUsersByCountry(
    @Param('countryId') countryId: string,
  ): Promise<ResponseUserDTO[]> {
    return await this.usersService.getUsersByCountry(+countryId);
  }

  @Put(':id/projects/:projectId')
  @UseGuards(BlacklistGuard, new RolesGuard(UserRole.Admin))
  async assignProject(
    @Param('id') userId: string,
    @Param('projectId') projectId: string,
  ): Promise<{ message: string }> {
    return await this.usersService.assignProject(+userId, +projectId);
  }

  @Get(':id/vacation')
  @UseGuards(BlacklistGuard)
  async getVacation(
    @Param('id') userId: string,
  ): Promise<ResponseVacationDTO[]> {
    return await this.usersService.getVacation(+userId);
  }

  @Post('vacation')
  @UseGuards(BlacklistGuard)
  async createVacation(
    @Body(new ValidationPipe({ whitelist: true }))
    vacationDTO: CreateVacationDTO,
    @UserId() loggedUserId: number,
  ): Promise<ResponseVacationDTO> {
    return await this.usersService.createVacation(vacationDTO, loggedUserId);
  }

  @Put('vacation/:id')
  @UseGuards(BlacklistGuard)
  async editVacation(
    @Body(new ValidationPipe({ whitelist: true }))
    vacationDTO: CreateVacationDTO,
    @Param('id') vacationId: string,
  ): Promise<ResponseVacationDTO> {
    return await this.usersService.editVacation(+vacationId, vacationDTO);
  }

  @Delete('vacation/:id')
  @UseGuards(BlacklistGuard)
  async deleteVacation(
    @UserId() loggedUserId: number,
    @Param('id') vacationId: string,
  ): Promise<{ message: string }> {
    return await this.usersService.deleteVacation(+vacationId, +loggedUserId);
  }
}
