import { IsNotEmpty } from 'class-validator';

export class CreateVacationDTO {
  @IsNotEmpty()
  startDate: Date;

  @IsNotEmpty()
  endDate: Date;
}
