import { UserRole } from 'src/common/enums/user-role';

export class ResponseUserDTO {
  public id: number;
  public username: string;
  public fullName: string;
  public email: string;
  public country: number;
  public role: UserRole;
  public project: number;
  public currentStatus: number;
  public nextPeriodStatus: number;
}
