import { UsersController } from './controller/users.controller';
import { Country } from '../country/model/country.entity';
import { CommonModule } from '../common/common.module';
import { UsersService } from './service/users.service';
import { Vacation } from './model/vacation.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './model/user.entity';
import { Module } from '@nestjs/common';

@Module({
  imports: [TypeOrmModule.forFeature([User, Country, Vacation]), CommonModule],
  controllers: [UsersController],
  providers: [UsersService],
  exports: [UsersService],
})
export class UsersModule {}
