import { BlacklistGuard } from './auth/guards/blacklist.guard';
import { PlanningModule } from './planning/planning.module';
import { AuthService } from './auth/services/auth.service';
import { ProjectModule } from './project/project.module';
import { CountryModule } from './country/country.module';
import { HistoryModule } from './history/history.module';
import { CommonModule } from './common/common.module';
import { UsersModule } from './users/users.module';
import { ScheduleModule } from '@nestjs/schedule';
import { AuthModule } from './auth/auth.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ApiModule } from './api/api.module';
import { Module } from '@nestjs/common';

@Module({
  imports: [
    AuthModule,
    UsersModule,
    ApiModule,
    PlanningModule,
    HistoryModule,
    CountryModule,
    ProjectModule,
    CommonModule,
    ScheduleModule.forRoot(),
    TypeOrmModule.forRoot({
      type: 'mariadb',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: 'root',
      database: 'coviddb',
      entities: ['dist/**/*.entity{.ts,.js}'],
      synchronize: true,
    }),
  ],
  providers: [
    {
      provide: AuthService,
      useClass: BlacklistGuard,
    },
  ],
})
export class AppModule {}
