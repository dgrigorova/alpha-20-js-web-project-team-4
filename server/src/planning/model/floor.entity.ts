import { Country } from '../../country/model/country.entity';
import {
  PrimaryGeneratedColumn,
  Entity,
  Column,
  ManyToOne,
  OneToOne,
  JoinColumn,
} from 'typeorm';

@Entity('floor')
export class Floor {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column('longtext')
  matrix: string;

  @Column('longtext', { nullable: true })
  matrixCurrentPeriod: string;

  @Column('longtext', { nullable: true })
  matrixNextPeriod: string;

  @JoinColumn()
  @OneToOne(
    type => Country,
    country => country.floor,
  )
  country: number;

  @Column({ default: false })
  isDeleted: boolean;
}
