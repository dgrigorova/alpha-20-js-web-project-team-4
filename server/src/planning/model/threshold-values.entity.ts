import { PrimaryGeneratedColumn, Entity, Column, ManyToOne } from 'typeorm';

@Entity('threshold_values')
export class ThresholdValues {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    nullable: true,
  })
  lowerLimit: number;

  @Column({
    nullable: true,
  })
  upperLimit: number;
}
