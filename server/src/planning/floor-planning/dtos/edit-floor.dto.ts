import { IsString, IsNotEmpty, IsArray, IsNumber } from 'class-validator';

export class EditFloorDTO {
  @IsString()
  @IsNotEmpty()
  public name: string;

  @IsArray()
  @IsNotEmpty()
  public matrix: Array<Array<number>>;

  @IsNumber()
  @IsNotEmpty()
  public country: number;
}
