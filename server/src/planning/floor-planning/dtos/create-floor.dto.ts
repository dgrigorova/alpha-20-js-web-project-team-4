import { IsString, IsNotEmpty, IsNumber, Min, Max } from 'class-validator';

export class CreateFloorDTO {
  @IsString()
  @IsNotEmpty()
  public name: string;

  @IsNumber()
  @IsNotEmpty()
  @Min(10)
  @Max(50)
  public rows: number;

  @IsNumber()
  @IsNotEmpty()
  @Min(10)
  @Max(50)
  public columns: number;

  @IsNumber()
  @IsNotEmpty()
  public country: number;
}
