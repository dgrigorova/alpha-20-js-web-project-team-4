export class ServerSideFloor {
  public id: number;
  public name: string;
  public matrix: Array<Array<number>>;
  public matrixCurrentPeriod: Array<Array<number>>;
  public matrixNextPeriod: Array<Array<number>>;
  public country: number;
  public isDeleted: boolean;
}
