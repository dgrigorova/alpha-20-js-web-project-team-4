export class AdditionalFloorInfo {
  public desks;
  public availableDesks;
  public forbiddenDesks;
}
