import { AdditionalFloorInfo } from './additional-info.dto';

export class ResponseFloorDTO {
  public id: number;
  public name: string;
  public matrix: Array<Array<number>>;
  public matrixCurrentPeriod: Array<Array<number>>;
  public matrixNextPeriod: Array<Array<number>>;
  public country: number;
  public additionalData: Partial<AdditionalFloorInfo>;
}
