import { EmployeesPlanningService } from '../../employees-planning/employees-planning.service';
import { FloorElementStatus } from './../../../common/enums/floor-element-status';
import { TransformService } from '../../../common/services/transform.service';
import { AdditionalFloorInfo } from './../dtos/additional-info.dto';
import { FindService } from '../../../common/services/find.service';
import { BadRequestException, Injectable } from '@nestjs/common';
import { UserStatus } from '../../../common/enums/user-status';
import { ResponseFloorDTO } from '../dtos/response-floor.dto';
import { CreateFloorDTO } from '../dtos/create-floor.dto';
import { User } from '../../../users/model/user.entity';
import { EditFloorDTO } from '../dtos/edit-floor.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Floor } from '../../model/floor.entity';
import { Repository } from 'typeorm';

@Injectable()
export class FloorService {
  constructor(
    @InjectRepository(Floor)
    private readonly floorRepository: Repository<Floor>,
    @InjectRepository(User)
    private readonly usersRepository: Repository<User>,
    private readonly transformService: TransformService,
    private readonly employeesPlanningService: EmployeesPlanningService,
    private readonly findService: FindService,
  ) {}

  async create(floorDTO: CreateFloorDTO): Promise<ResponseFloorDTO> {
    const country = await this.findService.getCountryById(floorDTO.country);

    const duplicateFloor = await this.floorRepository.findOne({
      where: {
        country: country.id,
      },
    });

    if (duplicateFloor) {
      throw new BadRequestException(`No duplicate floors allowed.`);
    }

    const floor = new Floor();

    const matrix = this.generateMatrix(floorDTO.rows, floorDTO.columns);

    const matrixCurrentPeriod = this.generateMatrix(
      floorDTO.rows,
      floorDTO.columns,
    );
    const matrixNextPeriod = this.generateMatrix(
      floorDTO.rows,
      floorDTO.columns,
    );

    const floorToSave = this.transformService.toSaveFloorInDB({
      ...floor,
      name: floorDTO.name,
      country: country.id,
      matrix,
      matrixCurrentPeriod,
      matrixNextPeriod,
    });

    const floorSaved = await this.floorRepository.save(floorToSave);

    const floorResponse = await this.findService.getFloorById(floorSaved.id);

    return this.transformService.toResponseFloorDTO(floorResponse, {
      ...this.getFloorStats(matrix),
    });
  }

  async getAll(): Promise<ResponseFloorDTO[]> {
    const floors = await this.floorRepository.find({
      relations: ['country'],
    });

    return floors.map(floor => {
      const serverSideFloor = this.transformService.toServerSideFloor(floor);

      const additionalInfo: Partial<AdditionalFloorInfo> = {
        ...this.getFloorStats(serverSideFloor.matrix),
      };

      return this.transformService.toResponseFloorDTO(floor, additionalInfo);
    });
  }

  async getFloor(floorId: number): Promise<ResponseFloorDTO> {
    const floor = await this.findService.getFloorById(floorId);

    const serverSideFloor = this.transformService.toServerSideFloor(floor);

    const additionalInfo: Partial<AdditionalFloorInfo> = {
      ...this.getFloorStats(serverSideFloor.matrix),
    };

    return this.transformService.toResponseFloorDTO(floor, additionalInfo);
  }

  async editFloor(floorId: number, floorDTO: EditFloorDTO) {
    await this.validateFloor(floorDTO, floorId);

    const floorForTheDB = this.transformService.toSaveFloorInDB(floorDTO);

    await this.floorRepository.update(floorId, {
      ...floorDTO,
      matrix: JSON.stringify(floorDTO.matrix),
    });

    const updatedFloor = await this.findService.getFloorById(floorId);

    const serverSideFloor = this.transformService.toServerSideFloor(
      updatedFloor,
    );

    const additionalInfo: Partial<AdditionalFloorInfo> = {
      ...this.getFloorStats(serverSideFloor.matrix),
    };

    return this.transformService.toResponseFloorDTO(
      updatedFloor,
      additionalInfo,
    );
  }

  async generateFloorPlanning(floorId: number): Promise<ResponseFloorDTO> {
    const { spacing } = { spacing: 1 };

    const availableDesk = FloorElementStatus.Available;
    const forbiddenDesk = FloorElementStatus.Forbidden;

    const floor = await this.findService.getFloorById(floorId);
    const serverSideFloor = this.transformService.toServerSideFloor(floor);

    const floorMatrix = serverSideFloor.matrix;
    const newMatrix = floorMatrix.map((row, rowIndex) => {
      const newRow = row.map((cell, columnIndex) => {
        const northCell = floorMatrix[rowIndex - spacing]?.[columnIndex];
        const southCell = floorMatrix[rowIndex + spacing]?.[columnIndex];
        const eastCell = floorMatrix[rowIndex][columnIndex + spacing];
        const westCell = floorMatrix[rowIndex][columnIndex - spacing];

        if (cell === availableDesk) {
          if (northCell === availableDesk)
            [(floorMatrix[rowIndex - spacing][columnIndex] = forbiddenDesk)];
          if (southCell === availableDesk)
            [(floorMatrix[rowIndex + spacing][columnIndex] = forbiddenDesk)];
          if (eastCell === availableDesk)
            [(floorMatrix[rowIndex][columnIndex + spacing] = forbiddenDesk)];
          if (westCell === availableDesk)
            [(floorMatrix[rowIndex][columnIndex - spacing] = forbiddenDesk)];
        }

        return cell;
      });

      return newRow;
    });

    await this.floorRepository.update(
      floor.id,
      this.transformService.toSaveFloorInDB({
        ...floor,
        matrix: newMatrix,
        matrixCurrentPeriod: serverSideFloor.matrixCurrentPeriod,
        matrixNextPeriod: serverSideFloor.matrixNextPeriod,
      }),
    );

    const updatedFloor = await this.findService.getFloorById(floorId);

    const additionalInfo: Partial<AdditionalFloorInfo> = {
      ...this.getFloorStats(newMatrix),
    };

    return this.transformService.toResponseFloorDTO(
      updatedFloor,
      additionalInfo,
    );
  }

  public async chooseSeatsForTheEmployees(floorId: number) {
    const availableSeat = FloorElementStatus.Available;

    const floor = await this.findService.getFloorById(floorId);

    const serverSideFloor = this.transformService.toServerSideFloor(floor);

    const sortedUsersCurrentPeriod = await this.employeesPlanningService.chooseEmployeesForOfficeCurrentPeriod(
      (serverSideFloor.country as any).id,
    );
    console.log(sortedUsersCurrentPeriod.map(i => i.currentStatus).join(', '));

    const setCurrentUserStatus = async (userId: number, status: UserStatus) => {
      await this.usersRepository
        .createQueryBuilder()
        .update(User)
        .set({ currentStatus: status })
        .where('id = :id', { id: userId })
        .execute();
    };

    const setNextPeriodUserStatus = async (
      userId: number,
      status: UserStatus,
    ) => {
      await this.usersRepository
        .createQueryBuilder()
        .update(User)
        .set({ nextPeriodStatus: status })
        .where('id = :id', { id: userId })
        .execute();
    };

    const iterateOverMatrix = (
      users: User[],
      func: (id: number, status: UserStatus) => Promise<void>,
    ) => {
      return serverSideFloor.matrix.map(row => {
        return row.map(cell => {
          if (cell === availableSeat && users.length) {
            const user = users.pop();

            func(user.id, UserStatus.Office);

            return user.id;
          }

          return cell;
        });
      });
    };

    const firstPeriodMatrix = await iterateOverMatrix(
      sortedUsersCurrentPeriod,
      setCurrentUserStatus,
    );

    await sortedUsersCurrentPeriod.map(async item => {
      return setCurrentUserStatus(item.id, UserStatus.Home);
    });

    const sortedUsersNextPeriod = await this.employeesPlanningService.chooseEmployeesForOfficeNextPeriod(
      (serverSideFloor.country as any).id,
    );
    console.log(sortedUsersNextPeriod.map(i => i.currentStatus).join(', '));

    const secondPeriodMatrix = await iterateOverMatrix(
      sortedUsersNextPeriod,
      setNextPeriodUserStatus,
    );

    await sortedUsersNextPeriod.map(async item => {
      return setNextPeriodUserStatus(item.id, UserStatus.Home);
    });

    await this.floorRepository.update(
      floor.id,
      this.transformService.toSaveFloorInDB({
        ...floor,
        matrix: serverSideFloor.matrix,
        matrixCurrentPeriod: firstPeriodMatrix,
        matrixNextPeriod: secondPeriodMatrix,
      }),
    );

    const updatedFloor = await this.findService.getFloorById(floorId);

    const additionalInfo: Partial<AdditionalFloorInfo> = {
      ...this.getFloorStats(serverSideFloor.matrix),
    };

    return this.transformService.toResponseFloorDTO(
      updatedFloor,
      additionalInfo,
    );
  }

  private getFloorStats(floorMatrix: number[][]): Partial<AdditionalFloorInfo> {
    let desks = 0;
    let availableDesks = 0;
    let forbiddenDesks = 0;

    floorMatrix.forEach(row => {
      row.forEach(cell => {
        if (cell === FloorElementStatus.Available) {
          desks++;
          availableDesks++;
        }
        if (cell === FloorElementStatus.Forbidden) {
          desks++;
          forbiddenDesks++;
        }
        if (cell > 0) {
          desks++;
        }
      });
    });

    return {
      desks,
      availableDesks,
      forbiddenDesks,
    };
  }
  private generateMatrix(rows: number, columns: number): number[][] {
    const matrix = Array.from({ length: rows }, array => {
      array = Array.from({ length: columns }, () => 0);
      return array;
    });

    return matrix as number[][];
  }

  /**
   * Validates any item if pass the validation rules.
   *
   * @param  floorToValidate
   * @param  referenceFloorId Finds the floor in the database
   *
   * @returns
   *
   * If the validation fails, throws an exception.
   */
  private async validateFloor(floorToValidate, referenceFloorId: number) {
    const floorToValidateFrom = await this.findService.getFloorById(
      referenceFloorId,
    );
    const transformedFloor = this.transformService.toServerSideFloor(
      floorToValidateFrom,
    );

    const validateCells = (cell, rowIndex, columnIndex) => {
      if (!cell && cell !== 0) {
        throw new BadRequestException(
          `Forbidden floor element - ${cell} at position ${rowIndex} : ${columnIndex}`,
        );
      }

      if (typeof cell !== 'number') {
        throw new BadRequestException(
          `Forbidden floor element - ${cell} at position ${rowIndex} : ${columnIndex}`,
        );
      }
    };

    const validateRow = (row, rowIndex) => {
      if (row.length === transformedFloor.matrix[rowIndex]?.length) {
        row.forEach((cell, columnIndex) =>
          validateCells(cell, rowIndex, columnIndex),
        );
      } else {
        throw new BadRequestException(
          `The floor cannot be different size (east - west) at ${rowIndex}`,
        );
      }
    };

    if (floorToValidate.matrix.length === transformedFloor.matrix.length) {
      floorToValidate.matrix.forEach((row, rowIndex) =>
        validateRow(row, rowIndex),
      );
    } else {
      throw new BadRequestException(
        `The floor cannot be different size (south - north)`,
      );
    }
  }
}
