import { EmployeesPlanningService } from '../../employees-planning/employees-planning.service';
import { TransformService } from '../../../common/services/transform.service';
import { FindService } from '../../../common/services/find.service';
import { User } from '../../../users/model/user.entity';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Floor } from '../../model/floor.entity';
import { FloorService } from './floor.service';

describe('FloorService', () => {
  let service: FloorService;

  const floorRepository = {
    findOne() {
      /* empty */
    },
    update() {
      /* empty */
    },
    save() {
      /* empty */
    },
  };

  const usersRepository = {
    createQueryBuilder() {
      /* empty */
    },
  };

  const transformService = {
    toResponseFloorDTO() {
      /* empty */
    },
  };

  const employeesService = {
    chooseEmployeesForTheOffice() {
      /* empty */
    },
  };

  const findService = {
    getCountryById() {
      /* empty */
    },
    getFloorById() {
      /* empty */
    },
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        FloorService,
        {
          provide: getRepositoryToken(Floor),
          useValue: floorRepository,
        },
        {
          provide: getRepositoryToken(User),
          useValue: usersRepository,
        },
        {
          provide: TransformService,
          useValue: transformService,
        },
        {
          provide: EmployeesPlanningService,
          useValue: employeesService,
        },
        {
          provide: FindService,
          useValue: findService,
        },
      ],
    }).compile();

    service = module.get<FloorService>(FloorService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
