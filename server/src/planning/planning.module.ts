import { EmployeesPlanningService } from './employees-planning/employees-planning.service';
import { FloorService } from './floor-planning/services/floor.service';
import { PlanningController } from './controllers/planning.controller';
import { ThresholdValues } from './model/threshold-values.entity';
import { HistoryModule } from './../history/history.module';
import { Country } from '../country/model/country.entity';
import { CommonModule } from './../common/common.module';
import { AuthModule } from './../auth/auth.module';
import { User } from '../users/model/user.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Floor } from './model/floor.entity';
import { Module } from '@nestjs/common';

@Module({
  imports: [
    TypeOrmModule.forFeature([Floor, Country, User, ThresholdValues]),
    CommonModule,
    HistoryModule,
    AuthModule,
  ],
  providers: [FloorService, EmployeesPlanningService],
  controllers: [PlanningController],
  exports: [FloorService],
})
export class PlanningModule {}
