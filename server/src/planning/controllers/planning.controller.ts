import { EmployeesPlanningService } from '../employees-planning/employees-planning.service';
import { ResponseFloorDTO } from '../floor-planning/dtos/response-floor.dto';
import { CreateFloorDTO } from '../floor-planning/dtos/create-floor.dto';
import { ThresholdDTO } from '../employees-planning/dtos/threshold.dto';
import { FloorService } from '../floor-planning/services/floor.service';
import { EditFloorDTO } from '../floor-planning/dtos/edit-floor.dto';
import { BlacklistGuard } from '../../auth/guards/blacklist.guard';
import { RolesGuard } from '../../auth/guards/roles.guard';
import { UserRole } from '../../common/enums/user-role';
import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Put,
  UseGuards,
  ValidationPipe,
} from '@nestjs/common';

@Controller('planning')
export class PlanningController {
  constructor(
    private readonly floorService: FloorService,
    private readonly employeeService: EmployeesPlanningService,
  ) {}

  @Post('floors')
  @UseGuards(BlacklistGuard, new RolesGuard(UserRole.Admin))
  async createFloor(
    @Body(new ValidationPipe({ whitelist: true })) floorDTO: CreateFloorDTO,
  ): Promise<ResponseFloorDTO> {
    return await this.floorService.create(floorDTO);
  }

  @Get('floors')
  async getAllFloors(): Promise<ResponseFloorDTO[]> {
    return await this.floorService.getAll();
  }

  @Get('floors/:id')
  async getFloor(@Param('id') floorId: string): Promise<ResponseFloorDTO> {
    this.employeeService.chooseEmployeesForOfficeCurrentPeriod(19);
    return await this.floorService.getFloor(+floorId);
  }

  @Put('floors/:id')
  @UseGuards(BlacklistGuard, new RolesGuard(UserRole.Admin))
  async editFloor(
    @Param('id') floorId: string,
    @Body(new ValidationPipe({ whitelist: true })) floorDTO: EditFloorDTO,
  ): Promise<ResponseFloorDTO> {
    return await this.floorService.editFloor(+floorId, floorDTO);
  }

  @Put('floors/:id/available-desks')
  @UseGuards(BlacklistGuard, new RolesGuard(UserRole.Admin))
  async generateFloorPlanning(@Param('id') floorId: string) {
    return await this.floorService.generateFloorPlanning(+floorId);
  }

  @Put('floors/:id/next-period')
  @UseGuards(BlacklistGuard, new RolesGuard(UserRole.Admin))
  async chooseSeatsForTheEmployees(@Param('id') floorId: string) {
    return this.floorService.chooseSeatsForTheEmployees(+floorId);
  }

  @Put('threshold-values')
  @UseGuards(BlacklistGuard, new RolesGuard(UserRole.Admin))
  async changeThresholdValues(
    @Body(new ValidationPipe({ whitelist: true }))
    thresholdDTO: Partial<ThresholdDTO>,
  ) {
    return await this.employeeService.changeThresholdValues(thresholdDTO);
  }
}
