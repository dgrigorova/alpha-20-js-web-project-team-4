import { IsNumber, IsPositive } from 'class-validator';

export class ThresholdDTO {
  @IsNumber()
  @IsPositive()
  public lowerLimit: number;

  @IsNumber()
  @IsPositive()
  public upperLimit: number;
}
