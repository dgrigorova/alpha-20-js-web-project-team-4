import { ThresholdValues } from './../model/threshold-values.entity';
import { EmployeesPlanningService } from './employees-planning.service';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { User } from '../../users/model/user.entity';

describe('EmployeesPlanningService', () => {
  let service: EmployeesPlanningService;

  const thresholdValuesRepository = {
    findOne() {
      /* empty */
    },
    update() {
      /* empty */
    },
  };

  const usersRepository = {
    createQueryBuilder() {
      /* empty */
    },
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        EmployeesPlanningService,
        {
          provide: getRepositoryToken(ThresholdValues),
          useValue: thresholdValuesRepository,
        },
        {
          provide: getRepositoryToken(User),
          useValue: usersRepository,
        },
      ],
    }).compile();

    service = module.get<EmployeesPlanningService>(EmployeesPlanningService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
