import { ThresholdValues } from './../model/threshold-values.entity';
import { Injectable, BadRequestException } from '@nestjs/common';
import { UserStatus } from '../../common/enums/user-status';
import { User } from './../../users/model/user.entity';
import { ThresholdDTO } from './dtos/threshold.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class EmployeesPlanningService {
  constructor(
    @InjectRepository(ThresholdValues)
    private readonly thresholdValuesRepository: Repository<ThresholdValues>,
    @InjectRepository(User)
    private readonly usersRepository: Repository<User>,
  ) {}

  async changeThresholdValues(thresholdDTO: Partial<ThresholdDTO>) {
    const currentValues = await this.thresholdValuesRepository.findOne();

    const newValues = {
      ...currentValues,
      ...thresholdDTO,
    };

    if (
      newValues.upperLimit >= 100 ||
      newValues.lowerLimit >= newValues.upperLimit
    ) {
      throw new BadRequestException(
        `The lower limit shouldn't be higher or equal than the upper limit & upper limit should be lower than or equal to 100`,
      );
    }

    await this.thresholdValuesRepository.update(currentValues.id, newValues);

    return this.thresholdValuesRepository.findOne();
  }

  public async chooseEmployeesForOfficeCurrentPeriod(
    countryId: number,
  ): Promise<User[]> {
    const users = await this.getEmployeesNotInVacation(countryId);
    const sortedUsers = [...users].sort((a, b) => {
      if (b.currentStatus === a.currentStatus) {
        return b.nextPeriodStatus - a.nextPeriodStatus;
      } else {
        return b.currentStatus - a.currentStatus;
      }
    });

    return sortedUsers;
  }

  public async chooseEmployeesForOfficeNextPeriod(
    countryId: number,
  ): Promise<User[]> {
    const users = await this.getEmployeesNotInVacation(countryId);
    const sortedUsers = [...users].sort((a, b) => {
      if (b.nextPeriodStatus === a.nextPeriodStatus) {
        return b.currentStatus - a.currentStatus;
      } else {
        return b.nextPeriodStatus - a.nextPeriodStatus;
      }
    });

    return sortedUsers;
  }

  private async getEmployeesNotInVacation(countryId: number): Promise<User[]> {
    const users: User[] = await this.usersRepository
      .createQueryBuilder('user')
      .select('*')
      .where('currentStatus != :currentStatus', {
        currentStatus: UserStatus.Vacation,
      })
      .andWhere('countryId = :countryId', { countryId })
      .getRawMany();

    if (!users) {
      throw new BadRequestException(
        `No data that match the request is existing.`,
      );
    }

    return users;
  }
}
