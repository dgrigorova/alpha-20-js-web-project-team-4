import { UsersController } from '../users/controller/users.controller';
import { AuthController } from './controller/auth-controller';
import { AuthService } from './services/auth.service';
import { JwtStrategy } from './strategy/jwt-strategy';
import { UsersModule } from '../users/users.module';
import { User } from './../users/model/user.entity';
import { jwtConstants } from '../constants/secret';
import { PassportModule } from '@nestjs/passport';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Token } from './models/token.entity';
import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';

@Module({
  imports: [
    UsersModule,
    PassportModule,
    TypeOrmModule.forFeature([User, Token]),
    JwtModule.register({
      secret: jwtConstants.secret,
      signOptions: {
        expiresIn: '7d',
      },
    }),
  ],
  controllers: [AuthController, UsersController],
  providers: [AuthService, JwtStrategy],
  exports: [AuthService],
})
export class AuthModule {}
