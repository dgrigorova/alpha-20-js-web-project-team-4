import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UserRole } from '../../common/enums/user-role';
import { User } from '../../users/model/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { JwtPayload } from '../../auth/jwt-payload';
import { Token } from '../models/token.entity';
import { JwtService } from '@nestjs/jwt';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    @InjectRepository(Token)
    private readonly tokenRepository: Repository<Token>,
    private readonly jwtService: JwtService,
  ) {}

  async findUserByName(username: string) {
    return await this.userRepository.findOne({
      where: {
        username,
      },
    });
  }

  async findUserByEmail(email: string) {
    return await this.userRepository.findOne({
      where: {
        email,
      },
    });
  }

  async blacklist(token: string) {
    const tokenEntity = this.tokenRepository.create();
    tokenEntity.token = token;

    await this.tokenRepository.save(tokenEntity);
  }

  async isBlacklisted(token: string): Promise<boolean> {
    return Boolean(
      await this.tokenRepository.findOne({
        where: {
          token,
        },
      }),
    );
  }

  async validateUser(email: string, password: string) {
    const user = await this.findUserByEmail(email);
    if (!user) {
      return null;
    }

    const isUserValidated = await bcrypt.compare(password, user.password);
    return isUserValidated ? user : null;
  }

  async login(email: string, password: string) {
    const user = await this.validateUser(email, password);

    if (!user) {
      throw new UnauthorizedException(`Wrong credentials!`);
    }

    const payload: JwtPayload = {
      id: user.id,
      username: user.username,
      role: UserRole[user.role],
    };

    const token = await this.jwtService.signAsync(payload);

    return {
      token,
    };
  }
}
