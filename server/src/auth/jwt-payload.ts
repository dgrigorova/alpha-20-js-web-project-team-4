export class JwtPayload {
  id: number;
  username: string;
  role: string;
}
