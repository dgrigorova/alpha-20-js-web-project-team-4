import { AuthService } from '../services/auth.service';
import { jwtConstants } from '../../constants/secret';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { JwtPayload } from '../../auth/jwt-payload';
import { Injectable } from '@nestjs/common';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly authService: AuthService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: jwtConstants.secret,
    });
  }

  async validate(payload: JwtPayload) {
    const user = await this.authService.findUserByName(payload.username);

    if (!user) {
      return;
    }

    if (user.banEndDate && user.banEndDate.valueOf() > Date.now()) {
      return;
    }

    return user;
  }
}
