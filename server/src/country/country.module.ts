import { ThresholdValues } from './../planning/model/threshold-values.entity';
import { CommonModule } from './../common/common.module';
import { HistoryModule } from './../history/history.module';
import { History } from './../history/model/countries.entity';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Country } from './model/country.entity';
import { CountryController } from './controller/country.controller';
import { CountryService } from './services/country.service';
import { TransformService } from 'src/common/services/transform.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Country, History, ThresholdValues]),
    HistoryModule,
    CommonModule,
  ],
  controllers: [CountryController],
  providers: [CountryService],
  exports: [CountryService],
})
export class CountryModule {}
