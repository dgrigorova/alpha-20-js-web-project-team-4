import { ResponseCountryDTO } from '../dtos/response-country-dto';
import { CountryService } from '../services/country.service';
import { Controller, Get, Param } from '@nestjs/common';

@Controller('countries')
export class CountryController {
  constructor(private readonly countryService: CountryService) {}

  @Get()
  async getAllCountries(): Promise<ResponseCountryDTO[]> {
    return await this.countryService.getAll();
  }

  @Get(':id/safety-rules')
  async getSafetyRules(@Param('id') countryId: number) {
    return await this.countryService.getSafetyRules(countryId);
  }
}
