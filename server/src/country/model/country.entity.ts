import { Project } from '../../project/model/project.entity';
import { Floor } from '../../planning/model/floor.entity';
import { User } from '../../users/model/user.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToOne,
  OneToMany,
} from 'typeorm';

@Entity('country')
export class Country {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('nvarchar', { unique: true })
  name: string;

  @OneToMany(
    type => Project,
    project => project.country,
  )
  project: Project[];

  @OneToMany(
    type => User,
    user => user.country,
  )
  employees: User[];

  @OneToOne(
    type => Floor,
    floor => floor.country,
  )
  floor: number;
}
