import { ThresholdValues } from '../../planning/model/threshold-values.entity';
import { TransformService } from '../../common/services/transform.service';
import { HistoryService } from '../../history/services/history.service';
import { officeSafetyRules, period } from '../../constants/constants';
import { ResponseCountryDTO } from '../dtos/response-country-dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Country } from '../model/country.entity';
import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';

@Injectable()
export class CountryService {
  constructor(
    @InjectRepository(Country)
    private readonly countryRepository: Repository<Country>,
    @InjectRepository(ThresholdValues)
    private readonly thresholdValuesRepository: Repository<ThresholdValues>,
    private readonly transformService: TransformService,
    private readonly historyService: HistoryService,
  ) {}
  async getAll(): Promise<ResponseCountryDTO[]> {
    const countries = await this.countryRepository.find();
    return countries.map(country =>
      this.transformService.toResponseCountryDTO(country),
    );
  }

  async getSafetyRules(
    countryId: number,
  ): Promise<{ percentEmployeesInOffice: number }> {
    const [country] = await this.countryRepository.findByIds([countryId]);
    const ratio = await this.historyService.calculateInfectedPerMillionTestsRatio(
      country.name,
      period,
    );

    const ratioPercentMultiplier = 100;
    const ratioInPercents = ratio * ratioPercentMultiplier;

    const thresholdValues = await this.thresholdValuesRepository.findOne();

    if (ratioInPercents >= thresholdValues.upperLimit) {
      return officeSafetyRules.valueAboveUpperLimit;
    }
    if (
      ratioInPercents < thresholdValues.upperLimit &&
      ratioInPercents >= thresholdValues.lowerLimit
    ) {
      return officeSafetyRules.valueAboveLowerLimit;
    }
    if (ratioInPercents < thresholdValues.lowerLimit) {
      return officeSafetyRules.valueBelowLowerLimit;
    }
  }
}
