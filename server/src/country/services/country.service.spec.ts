import { ThresholdValues } from '../../planning/model/threshold-values.entity';
import { TransformService } from '../../common/services/transform.service';
import { HistoryService } from '../../history/services/history.service';
import { Country } from '../../country/model/country.entity';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { CountryService } from './country.service';

describe('CountryService', () => {
  let service: CountryService;

  const countryRepository = {
    find() {
      /* empty */
    },
    findByIds() {
      /* empty */
    },
  };

  const thresholdValuesRepository = {
    find() {
      /* empty */
    },
  };

  const transformService = {
    toResponseCountryDTO() {
      /* empty */
    },
  };

  const historyService = {
    calculateInfectedPerMillionTestsRatio() {
      /* empty */
    },
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CountryService,
        {
          provide: getRepositoryToken(Country),
          useValue: countryRepository,
        },
        {
          provide: getRepositoryToken(ThresholdValues),
          useValue: thresholdValuesRepository,
        },
        {
          provide: TransformService,
          useValue: transformService,
        },
        {
          provide: HistoryService,
          useValue: historyService,
        },
      ],
    }).compile();

    service = module.get<CountryService>(CountryService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
