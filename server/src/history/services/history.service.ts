import { Injectable, BadRequestException } from '@nestjs/common';
import { History } from '../../history/model/countries.entity';
import { ONE_DAY_IN_MS } from '../../constants/constants';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class HistoryService {
  constructor(
    @InjectRepository(History)
    private readonly historyRepository: Repository<History>,
  ) {}

  async calculateInfectedPerMillionTestsRatio(
    countryName: string,
    period: number,
  ) {
    const diff = ONE_DAY_IN_MS * period;

    const currentDate = new Date().toISOString();

    const startDateInMs = new Date().getTime() - diff;
    const startDate = new Date(startDateInMs).toISOString();

    const yesterdayInMs = new Date().getTime() - ONE_DAY_IN_MS;
    const yesterday = new Date(yesterdayInMs).toISOString();

    const targetColumns = 'casesPerOneMillion, testsPerOneMillion';

    const earliestHistoryRecord: {
      casesPerOneMillion: number;
      testsPerOneMillion: number;
    } = await this.historyRepository
      .createQueryBuilder('history')
      .select(targetColumns)
      .where('date > :startDate', { startDate })
      .andWhere('countryName = :countryName', { countryName })
      .getRawOne();

    const latestHistoryRecord: {
      casesPerOneMillion: number;
      testsPerOneMillion: number;
    } = await this.historyRepository
      .createQueryBuilder('history')
      .select(targetColumns)
      .where('date < :currentDate', {
        currentDate,
      })
      .andWhere('date > :yesterday', { yesterday })
      .andWhere('countryName = :countryName', { countryName })
      .getRawOne();

    const InfectedPerMillionTestsRatio =
      (latestHistoryRecord?.casesPerOneMillion -
        earliestHistoryRecord?.casesPerOneMillion) /
      (latestHistoryRecord?.testsPerOneMillion -
        earliestHistoryRecord?.testsPerOneMillion);

    if (!InfectedPerMillionTestsRatio) {
      throw new BadRequestException(
        `Cannot find data from the last ${period} days for ${countryName}`,
      );
    }

    return InfectedPerMillionTestsRatio;
  }
}
