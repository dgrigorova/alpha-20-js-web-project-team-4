import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
} from 'typeorm';

@Entity('history')
export class History {
  @PrimaryGeneratedColumn()
  id: number;

  @CreateDateColumn()
  date: Date;

  @Column('nvarchar')
  countryName: string;

  @Column({ nullable: true })
  cases: number;

  @Column({ nullable: true })
  todayCases: number;

  @Column({ nullable: true })
  deaths: number;

  @Column({ nullable: true })
  todayDeaths: number;

  @Column({ nullable: true })
  recovered: number;

  @Column({ nullable: true })
  active: number;

  @Column({ nullable: true })
  critical: number;

  @Column({ nullable: true })
  casesPerOneMillion: number;

  @Column({ nullable: true })
  deathsPerOneMillion: number;

  @Column({ nullable: true })
  totalTests: number;

  @Column({ nullable: true })
  testsPerOneMillion: number;
}
