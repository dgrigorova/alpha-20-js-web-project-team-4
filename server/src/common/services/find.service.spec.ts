import { Vacation } from './../../users/model/vacation.entity';
import { Project } from '../../project/model/project.entity';
import { Country } from '../../country/model/country.entity';
import { Floor } from '../../planning/model/floor.entity';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { User } from '../../users/model/user.entity';
import { FindService } from './find.service';

describe('FindService', () => {
  let service: FindService;

  const usersRepository = {
    findOne() {
      /* empty */
    },
  };

  const floorRepository = {
    findOne() {
      /* empty */
    },
  };

  const countryRepository = {
    findOne() {
      /* empty */
    },
  };

  const vacationRepository = {
    findOne() {
      /* empty */
    },
  };

  const projectRepository = {
    findOne() {
      /* empty */
    },
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        FindService,
        {
          provide: getRepositoryToken(User),
          useValue: usersRepository,
        },
        {
          provide: getRepositoryToken(Floor),
          useValue: floorRepository,
        },
        {
          provide: getRepositoryToken(Country),
          useValue: countryRepository,
        },
        {
          provide: getRepositoryToken(Vacation),
          useValue: vacationRepository,
        },
        {
          provide: getRepositoryToken(Project),
          useValue: vacationRepository,
        },
      ],
    }).compile();

    service = module.get<FindService>(FindService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
