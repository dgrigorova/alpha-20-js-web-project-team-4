import { AdditionalFloorInfo } from '../../planning/floor-planning/dtos/additional-info.dto';
import { ServerSideFloor } from './../../planning/floor-planning/dtos/server-side-floor.dto';
import { ResponseFloorDTO } from '../../planning/floor-planning/dtos/response-floor.dto';
import { ResponseCountryDTO } from '../../country/dtos/response-country-dto';
import { ResponseVacationDTO } from '../../users/dtos/response-vacation-dto';
import { ResponseProjectDTO } from '../../project/dtos/response-project-dto';
import { ResponseUserDTO } from '../../users/dtos/response-user-dto';
import { Country } from '../../country/model/country.entity';
import { Project } from '../../project/model/project.entity';
import { Vacation } from '../../users/model/vacation.entity';
import { Floor } from '../../planning/model/floor.entity';
import { User } from '../../users/model/user.entity';
import { Injectable } from '@nestjs/common';

@Injectable()
export class TransformService {
  toResponseUserDTO(user: User): ResponseUserDTO {
    return {
      id: user.id,
      username: user.username,
      fullName: user.fullName,
      email: user.email,
      country: user.country,
      role: user.role,
      currentStatus: user.currentStatus,
      nextPeriodStatus: user.nextPeriodStatus,
      project: user.project,
    };
  }

  toServerSideFloor(floor: Floor): ServerSideFloor {
    return {
      id: floor.id,
      name: floor.name,
      matrix: JSON.parse(floor.matrix),
      matrixCurrentPeriod: JSON.parse(floor.matrixCurrentPeriod),
      matrixNextPeriod: JSON.parse(floor.matrixNextPeriod),
      country: floor.country,
      isDeleted: floor.isDeleted,
    };
  }
  toSaveFloorInDB(floor: Partial<ServerSideFloor>): Floor {
    return {
      id: floor.id,
      name: floor.name,
      matrix: JSON.stringify(floor.matrix),
      matrixCurrentPeriod: JSON.stringify(floor.matrixCurrentPeriod),
      matrixNextPeriod: JSON.stringify(floor.matrixNextPeriod),
      country: floor.country,
      isDeleted: floor.isDeleted,
    };
  }

  toResponseFloorDTO(
    floor: Floor,
    additionalData: Partial<AdditionalFloorInfo>,
  ): ResponseFloorDTO {
    return {
      id: floor.id,
      name: floor.name,
      matrix: JSON.parse(floor.matrix),
      matrixCurrentPeriod: JSON.parse(floor.matrixCurrentPeriod),
      matrixNextPeriod: JSON.parse(floor.matrixNextPeriod),
      country: floor.country,
      additionalData,
    };
  }

  toResponseCountryDTO(country: Country): ResponseCountryDTO {
    return {
      id: country.id,
      name: country.name,
    };
  }

  toResponseVacationDTO(vacation: Vacation): ResponseVacationDTO {
    return {
      id: vacation.id,
      startDate: vacation.startDate,
      endDate: vacation.endDate,
      user: vacation.user,
    };
  }

  toResponseProjectDTO(project: Project): ResponseProjectDTO {
    return {
      id: project.id,
      name: project.name,
      country: project.country,
    };
  }
}
