import { Country } from './../../country/model/country.entity';
import { Vacation } from '../../users/model/vacation.entity';
import { Project } from '../../project/model/project.entity';
import { Floor } from '../../planning/model/floor.entity';
import { User } from '../../users/model/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';

@Injectable()
export class FindService {
  constructor(
    @InjectRepository(User)
    private readonly usersRepository: Repository<User>,
    @InjectRepository(Floor)
    private readonly floorRepository: Repository<Floor>,
    @InjectRepository(Country)
    private readonly countryRepository: Repository<Country>,
    @InjectRepository(Vacation)
    private readonly vacationRepository: Repository<Vacation>,
    @InjectRepository(Project)
    private readonly projectRepository: Repository<Project>,
  ) {}

  /**
   * Finds one user by its ID.
   *
   * @param  userId
   *
   * @returns
   *
   * If fails, throws an exception.
   */
  public async getUserById(userId: number): Promise<User> {
    const user = await this.usersRepository.findOne({
      where: {
        id: userId,
        isDeleted: false,
      },
      relations: ['country', 'project'],
    });

    if (!user) {
      throw new BadRequestException(`No user with id ${userId} found!`);
    }

    return user;
  }

  public async getUsersByCountry(countryId: number): Promise<User[]> {
    const users = await this.usersRepository.find({
      where: {
        country: countryId,
        isDeleted: false,
      },
      relations: ['project'],
    });

    return users;
  }

  /**
   * Finds one floor by its ID.
   *
   * @param  floorId
   *
   * @returns
   *
   * If fails, trows an exception.
   */
  public async getFloorById(floorId: number): Promise<Floor> {
    const floor = await this.floorRepository.findOne({
      where: {
        id: floorId,
      },
      relations: ['country'],
    });

    if (!floor) {
      throw new BadRequestException(`No floor with id ${floorId} found!`);
    }

    return floor;
  }

  public async getCountryById(countryId: number) {
    const country = await this.countryRepository.findOne({
      where: {
        id: countryId,
      },
      relations: ['project', 'floor'],
    });

    if (!country) {
      throw new BadRequestException(`No country with id ${countryId} found!`);
    }

    return country;
  }

  public async getVacationById(vacationId: number) {
    const vacation = await this.vacationRepository.findOne({
      where: {
        id: vacationId,
      },
    });

    if (!vacation) {
      throw new NotFoundException(`No vacation with id ${vacationId} found!`);
    }

    return vacation;
  }

  public async getProjectById(projectId: number) {
    const project = await this.projectRepository.findOne({
      where: {
        id: projectId,
        isDeleted: false,
      },
      relations: ['country'],
    });

    if (!project) {
      throw new NotFoundException(`No project with id ${projectId} found!`);
    }

    return project;
  }
}
