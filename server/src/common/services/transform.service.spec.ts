import { Test, TestingModule } from '@nestjs/testing';
import { TransformService } from './transform.service';

describe('TransformService', () => {
  let service: TransformService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TransformService],
    }).compile();

    service = module.get<TransformService>(TransformService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('toResponseUserDTO', () => {
    it('should return an object', async () => {
      // Arrange
      const user = {
        id: 5,
        username: 'John',
        fullName: 'JohnDoe',
        password: 'string',
        email: 'john.doe@nest.js',
        status: 0,
        banEndDate: null,
        project: null,
        isDeleted: false,
        country: 15,
        role: 1,
        vacation: null,
        team: null,
      };

      const result = {
        id: 5,
        username: 'John',
        fullName: 'JohnDoe',
        email: 'john.doe@nest.js',
        country: 15,
        role: 1,
      };

      // Act
      jest
        .spyOn(service, 'toResponseUserDTO')
        .mockImplementation(user => result);

      // Assert
      expect(await service.toResponseUserDTO(user)).toBe(result);
    });
  });

  describe('toResponseFloorDTO', () => {
    it('should return an object', async () => {
      // Arrange
      const floor = {
        id: 5,
        name: 'BG',
        matrix: `[
          [0, 0, 0, 0, 0],
          [0, 0, 0, 0, 0],
          [0, 0, 0, 0, 0],
        ]`,
        country: 15,
      };

      const result = {
        id: 5,
        name: 'BG',
        matrix: [
          [0, 0, 0, 0, 0],
          [0, 0, 0, 0, 0],
          [0, 0, 0, 0, 0],
        ],
        country: 15,
      };

      // Act
      jest
        .spyOn(service, 'toResponseFloorDTO')
        .mockImplementation(floor => result);

      // Assert
      expect(await service.toResponseFloorDTO(floor)).toBe(result);
    });
  });

  describe('toResponseCountryDTO', () => {
    it('should return an object', async () => {
      // Arrange
      const country = {
        id: 5,
        name: 'USA',
        project: null,
        employees: null,
        team: null,
        floor: 5,
      };

      const result = {
        id: 5,
        name: 'USA',
      };

      // Act
      jest
        .spyOn(service, 'toResponseCountryDTO')
        .mockImplementation(country => result);

      // Assert
      expect(await service.toResponseCountryDTO(country)).toBe(result);
    });
  });
});
