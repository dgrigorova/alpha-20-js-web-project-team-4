import { TransformService } from './services/transform.service';
import { Country } from '../country/model/country.entity';
import { Project } from '../project/model/project.entity';
import { Vacation } from '../users/model/vacation.entity';
import { Floor } from '../planning/model/floor.entity';
import { FindService } from './services/find.service';
import { User } from '../users/model/user.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';

@Module({
  imports: [
    TypeOrmModule.forFeature([Floor, Country, User, Vacation, Project]),
  ],
  providers: [FindService, TransformService],
  controllers: [],
  exports: [FindService, TransformService],
})
export class CommonModule {}
