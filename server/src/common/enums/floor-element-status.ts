export enum FloorElementStatus {
  EmptySpace = 0,
  Available = -1,
  Forbidden = -2,
  OOOSpace = -3,
}
