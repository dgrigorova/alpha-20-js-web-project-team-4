export enum UserStatus {
  Unassigned = 1,
  Home = 2,
  Office = 3,
  Vacation = 4,
}
