import { Injectable, HttpService, HttpException } from '@nestjs/common';
import { History } from '../../history/model/countries.entity';
import { CronExpression, Cron } from '@nestjs/schedule';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class ApiService {
  constructor(
    private httpService: HttpService,

    @InjectRepository(History)
    private readonly countriesRepository: Repository<History>,
  ) {}

  @Cron(CronExpression.EVERY_DAY_AT_1PM)
  async getAllCountriesInfo() {
    try {
      const response = await this.httpService
        .get('https://coronavirus-19-api.herokuapp.com/countries')
        .toPromise();

      const allCountriesInfo = response.data;

      allCountriesInfo.map(async c => {
        const country = new History();

        country.countryName = c.country;
        country.cases = c.cases;
        country.todayCases = c.todayCases;
        country.deaths = c.deaths;
        country.todayDeaths = c.todayDeaths;
        country.recovered = c.recovered;
        country.active = c.active;
        country.critical = c.critical;
        country.casesPerOneMillion = c.casesPerOneMillion;
        country.deathsPerOneMillion = c.deathsPerOneMillion;
        country.totalTests = c.totalTests;
        country.testsPerOneMillion = c.testsPerOneMillion;

        await this.countriesRepository.save(country);
      });
    } catch (e) {
      throw new HttpException(e.response.data, e.response.status);
    }
  }

  async getInfoByCountryName(countryName: string) {
    try {
      const response = await this.httpService
        .get(
          `https://coronavirus-19-api.herokuapp.com/countries/${countryName}`,
        )
        .toPromise();
      return response.data;
    } catch (e) {
      throw new HttpException(e.response.data, e.response.status);
    }
  }
}
