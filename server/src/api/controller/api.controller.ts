import { Controller, Get } from '@nestjs/common';
import { Param } from '@nestjs/common/decorators/http/route-params.decorator';
import { ApiService } from '../services/api.service';

@Controller('countries')
export class ApiController {
  constructor(private readonly apiService: ApiService) {}

  @Get(':countryName')
  async getCountryInfo(@Param('countryName') countryName: string) {
    return this.apiService.getInfoByCountryName(countryName);
  }
}
