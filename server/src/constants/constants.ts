export const officeSafetyRules = {
  valueBelowLowerLimit: {
    percentEmployeesInOffice: 1,
  },
  valueAboveLowerLimit: {
    percentEmployeesInOffice: 0.75,
  },
  valueAboveUpperLimit: {
    percentEmployeesInOffice: 0.5,
  },
};

export const period = 7;

export const ONE_DAY_IN_MS = 86_400_000;
