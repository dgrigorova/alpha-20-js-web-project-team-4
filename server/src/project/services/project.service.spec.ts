import { TransformService } from '../../common/services/transform.service';
import { FindService } from '../../common/services/find.service';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Project } from './../model/project.entity';
import { ProjectService } from './project.service';

describe('ProjectService', () => {
  let service: ProjectService;

  const transformService = {
    toResponseProjectDTO() {
      /* empty */
    },
  };

  const projectRepository = {
    find() {
      /* empty */
    },
    create() {
      /* empty */
    },
    update() {
      /* empty */
    },
    save() {
      /* empty */
    },
  };

  const findService = {
    getProjectById() {
      /* empty */
    },
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ProjectService,
        {
          provide: getRepositoryToken(Project),
          useValue: projectRepository,
        },
        {
          provide: TransformService,
          useValue: transformService,
        },
        {
          provide: FindService,
          useValue: findService,
        },
      ],
    }).compile();

    service = module.get<ProjectService>(ProjectService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
