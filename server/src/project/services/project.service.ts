import { TransformService } from '../../common/services/transform.service';
import { ResponseProjectDTO } from '../dtos/response-project-dto';
import { FindService } from '../../common/services/find.service';
import { CreateProjectDTO } from '../dtos/create-project-dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Project } from '../model/project.entity';
import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';

@Injectable()
export class ProjectService {
  constructor(
    @InjectRepository(Project)
    private readonly projectRepository: Repository<Project>,
    private readonly transformService: TransformService,
    private readonly findService: FindService,
  ) {}

  async getAll(): Promise<ResponseProjectDTO[]> {
    const projects = await this.projectRepository.find({
      where: {
        isDeleted: false,
      },
      relations: ['country'],
    });

    return projects.map(project =>
      this.transformService.toResponseProjectDTO(project),
    );
  }

  async getById(projectId: number): Promise<ResponseProjectDTO> {
    const project = await this.findService.getProjectById(projectId);

    return this.transformService.toResponseProjectDTO(project);
  }

  async create(projectDTO: CreateProjectDTO): Promise<ResponseProjectDTO> {
    const project = this.projectRepository.create(projectDTO);
    const createdProject = await this.projectRepository.save(project);

    return this.transformService.toResponseProjectDTO(createdProject);
  }

  async update(
    projectDTO: Partial<Project>,
    id: number,
  ): Promise<ResponseProjectDTO> {
    await this.projectRepository.update(id, projectDTO);
    const updatedProject = await this.findService.getProjectById(id);

    return this.transformService.toResponseProjectDTO(updatedProject);
  }

  async delete(projectId: number): Promise<{ message: string }> {
    const project = await this.findService.getProjectById(projectId);

    project.isDeleted = true;
    await this.projectRepository.update(project.id, project);

    return {
      message: `Project with id ${projectId} is deleted.`,
    };
  }
}
