import { Country } from '../../country/model/country.entity';
import { User } from '../../users/model/user.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  JoinColumn,
  OneToMany,
  ManyToOne,
} from 'typeorm';

@Entity('project')
export class Project {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('nvarchar')
  name: string;

  @ManyToOne(
    type => Country,
    country => country.project,
  )
  @JoinColumn()
  country: number;

  @OneToMany(
    type => User,
    user => user.project,
  )
  assignedUsers: User[];

  @Column({ default: false })
  isDeleted: boolean;
}
