import { ResponseProjectDTO } from '../dtos/response-project-dto';
import { CreateProjectDTO } from '../dtos/create-project-dto';
import { ProjectService } from '../services/project.service';
import { Project } from '../model/project.entity';
import {
  Controller,
  Post,
  Param,
  Get,
  Body,
  ValidationPipe,
  Put,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { BlacklistGuard } from 'src/auth/guards/blacklist.guard';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { UserRole } from 'src/common/enums/user-role';

@Controller('projects')
export class ProjectController {
  constructor(private readonly projectService: ProjectService) {}

  @Get()
  async getAllProjects(): Promise<ResponseProjectDTO[]> {
    return await this.projectService.getAll();
  }

  @Post()
  @UseGuards(BlacklistGuard, new RolesGuard(UserRole.Admin))
  async createProject(
    @Body(new ValidationPipe({ whitelist: true })) projectDTO: CreateProjectDTO,
  ): Promise<ResponseProjectDTO> {
    return await this.projectService.create(projectDTO);
  }

  @Get(':id')
  async getProjectById(
    @Param('id') projectId: string,
  ): Promise<ResponseProjectDTO> {
    return this.projectService.getById(+projectId);
  }

  @Put(':id')
  @UseGuards(BlacklistGuard, new RolesGuard(UserRole.Admin))
  async updateProject(
    @Body(new ValidationPipe({ whitelist: true })) projectDTO: Partial<Project>,
    @Param('id') projectId: string,
  ): Promise<ResponseProjectDTO> {
    return await this.projectService.update(projectDTO, +projectId);
  }

  @Delete(':id')
  @UseGuards(BlacklistGuard, new RolesGuard(UserRole.Admin))
  async deleteProject(
    @Param('id') projectId: string,
  ): Promise<{ message: string }> {
    return await this.projectService.delete(+projectId);
  }
}
