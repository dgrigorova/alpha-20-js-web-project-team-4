import { Test, TestingModule } from '@nestjs/testing';
import { ProjectService } from '../services/project.service';
import { ProjectController } from './project.controller';

describe('ProjectController', () => {
  let controller: ProjectController;

  const projectService = {
    getAll() {
      /* empty */
    },
    create() {
      /* empty */
    },
    getById() {
      /* empty */
    },
    update() {
      /* empty */
    },
    delete() {
      /* empty */
    },
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ProjectController],
      providers: [
        {
          provide: ProjectService,
          useValue: projectService,
        },
      ],
    }).compile();

    controller = module.get<ProjectController>(ProjectController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
