import { IsString, IsNotEmpty, IsNumber } from 'class-validator';

export class CreateProjectDTO {
  @IsString()
  public name: string;

  @IsNotEmpty()
  public country: number;
}
