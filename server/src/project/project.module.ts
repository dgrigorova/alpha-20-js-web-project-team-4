import { TransformService } from '../common/services/transform.service';
import { ProjectController } from './controllers/project.controller';
import { ProjectService } from './services/project.service';
import { CommonModule } from '../common/common.module';
import { Project } from './model/project.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { AuthModule } from 'src/auth/auth.module';

@Module({
  imports: [TypeOrmModule.forFeature([Project]), CommonModule, AuthModule],
  controllers: [ProjectController],
  providers: [ProjectService, TransformService],
  exports: [ProjectService],
})
export class ProjectModule {}
