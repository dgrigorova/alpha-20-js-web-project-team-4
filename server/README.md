## Description

A web portal that helps multinational companies with hot desk policy to ease the returning of their employees back to the office after COVID-19 lockdown.

Main consideration that must be taken into account – **average number of newly infected people for the last week in the particular country**.

Number of infected people per country is dynamically gathered periodically from https://github.com/javieraviles/covidAPI

Phased return to the office strategy should be applied:

<!-- 1. If the ratio between that number and the number of tests per one million citizens in the particular country for the same period is > 10%, then max 50% of the employees can be at the office -->

<!-- 1. If the ratio is between 5% and 10%, then max 75% of the employees should work at the office -->

<!-- 1. If the ration is <5% everyone could go back at the office -->

<!-- **Note:** 5% and 10% are just default threshold values and they can be changed -->

### General workspace rules and assumption to follow:

1. One country = one workspace = one floor

1. Each project is allocated to one country only

1. A person can work only in one project

1. Each office has a different floor planning

1. Every week employees at the office should be rotated

<!-- 1. People working on the same project should be in the same time at the office -->

1. Some of the employees might be on vacation and no desk should be planned for them

1. For first two phases, employees should keep physical distance of 1.5m and not use desks next to each other (diagonal desks are allowed).

## Installation

```bash
$ npm install
```

## Documentation

```bash
# Compodoc
$ npx compodoc -p tsconfig.json -s
# npm 6 is required for npx support
```

Open your browser and navigate to http://localhost:8080.

## Database configuration

You should pass the username, password and database name in the **app.module.ts** file located at the root of the project.

```ts
TypeOrmModule.forRoot({
      type: 'mariadb',
      host: 'localhost',
      port: 3306,
      username: '',
      password: '',
      database: '',
      entities: ['dist/**/*.entity{.ts,.js}'],
      synchronize: true,
    }),
```

## Database seed

You should pass the username, password and database name in the **ormconfig.json** file located at the root of the project.

```json
{
  "type": "mariadb",
  "host": "localhost",
  "port": 3306,
  "username": "",
  "password": "",
  "database": "",
  "synchronize": true,
  "logging": false,
  "entities": ["./src/**/model/*.entity.ts"],

  "cli": {
    "entitiesDir": "src/model",
    "migrationsDir": "src/data/migration"
  }
}
```

Then run

```bash
$ npm run seed
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test
```

<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo_text.svg" width="320" alt="Nest Logo" /></a>
</p>

[travis-image]: https://api.travis-ci.org/nestjs/nest.svg?branch=master
[travis-url]: https://travis-ci.org/nestjs/nest
[linux-image]: https://img.shields.io/travis/nestjs/nest/master.svg?label=linux
[linux-url]: https://travis-ci.org/nestjs/nest

  <p align="center">A progressive <a href="http://nodejs.org" target="blank">Node.js</a> framework for building efficient and scalable server-side applications, heavily inspired by <a href="https://angular.io" target="blank">Angular</a>.</p>
    <p align="center">
<a href="https://www.npmjs.com/~nestjscore"><img src="https://img.shields.io/npm/v/@nestjs/core.svg" alt="NPM Version" /></a>
<a href="https://www.npmjs.com/~nestjscore"><img src="https://img.shields.io/npm/l/@nestjs/core.svg" alt="Package License" /></a>
<a href="https://www.npmjs.com/~nestjscore"><img src="https://img.shields.io/npm/dm/@nestjs/core.svg" alt="NPM Downloads" /></a>
<a href="https://travis-ci.org/nestjs/nest"><img src="https://api.travis-ci.org/nestjs/nest.svg?branch=master" alt="Travis" /></a>
<a href="https://travis-ci.org/nestjs/nest"><img src="https://img.shields.io/travis/nestjs/nest/master.svg?label=linux" alt="Linux" /></a>
<a href="https://coveralls.io/github/nestjs/nest?branch=master"><img src="https://coveralls.io/repos/github/nestjs/nest/badge.svg?branch=master#5" alt="Coverage" /></a>
<a href="https://gitter.im/nestjs/nestjs?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=body_badge"><img src="https://badges.gitter.im/nestjs/nestjs.svg" alt="Gitter" /></a>
<a href="https://opencollective.com/nest#backer"><img src="https://opencollective.com/nest/backers/badge.svg" alt="Backers on Open Collective" /></a>
<a href="https://opencollective.com/nest#sponsor"><img src="https://opencollective.com/nest/sponsors/badge.svg" alt="Sponsors on Open Collective" /></a>
  <a href="https://paypal.me/kamilmysliwiec"><img src="https://img.shields.io/badge/Donate-PayPal-dc3d53.svg"/></a>
</p>
  <!--[![Backers on Open Collective](https://opencollective.com/nest/backers/badge.svg)](https://opencollective.com/nest#backer)
  [![Sponsors on Open Collective](https://opencollective.com/nest/sponsors/badge.svg)](https://opencollective.com/nest#sponsor)-->
