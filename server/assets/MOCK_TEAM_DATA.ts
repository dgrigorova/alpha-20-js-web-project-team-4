export const MOCK_TEAM_DATA = [
  { id: 1, name: 'Extended' },
  { id: 2, name: 'Sharable' },
  { id: 3, name: 'toolset' },
  { id: 4, name: 'explicit' },
  { id: 5, name: 'Open-architected' },
  { id: 6, name: 'radical' },
  { id: 7, name: 'uniform' },
  { id: 8, name: 'user-facing' },
  { id: 9, name: 'Networked' },
  { id: 10, name: 'fresh-thinking' },
  { id: 11, name: 'toolset' },
  { id: 12, name: 'Managed' },
  { id: 13, name: 'Enhanced' },
  { id: 14, name: 'ability' },
  { id: 15, name: 'Vision-oriented' },
  { id: 16, name: 'Seamless' },
  { id: 17, name: 'solution' },
  { id: 18, name: 'clear-thinking' },
  { id: 19, name: 'even-keeled' },
  { id: 20, name: 'flexibility' },
  { id: 21, name: 'policy' },
  { id: 22, name: 'reciprocal' },
  { id: 23, name: 'Front-line' },
  { id: 24, name: 'Fundamental' },
  { id: 25, name: '6th generation' },
];
