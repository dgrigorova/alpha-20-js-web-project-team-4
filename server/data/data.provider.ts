import { UserRole } from '../src/common/enums/user-role';
import * as bcrypt from 'bcrypt';

export class DataProvider {
  private readonly _numberOfCountries: number = 20;
  private readonly _numberOfTeams: number = 25;
  private readonly _numberOfProjects: number = 60;
  private readonly _numberOfStatuses: number = 4;
  constructor(
    private readonly usersMockData: Array<{
      id: number;
      username: string;
      fullName: string;
      email: string;
    }>,
    private readonly teamsMockData: Array<{
      id: number;
      name: string;
    }>,
    private readonly projectsMockData: Array<{
      id: number;
      name: string;
    }>,
  ) {}
  async users() {
    const users = await this.generateUsers();
    return users;
  }

  teams() {
    return this.generateTeams();
  }

  projects() {
    return this.generateProjects();
  }

  private getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

  private async generateUsers() {
    const basicUserPassword = await bcrypt.hash('root', 10);
    const adminPassword = await bcrypt.hash('admin', 10);
    return await this.usersMockData.map((eachUser, index) => {
      const country =
        ((index + this._numberOfCountries) % this._numberOfCountries) + 1;
      const team = ((index + this._numberOfTeams) % this._numberOfTeams) + 1;
      const project =
        ((index + this._numberOfProjects) % this._numberOfProjects) + 1;
      const status = this.getRandomIntInclusive(1, this._numberOfStatuses);

      if (index === 1 || index === 2) {
        return {
          ...eachUser,
          password: adminPassword,
          role: UserRole.Admin,
          banEndDate: null,
          isDeleted: false,
          country,
          team,
          project,
          status,
        };
      } else {
        return {
          ...eachUser,
          password: basicUserPassword,
          role: UserRole.Basic,
          banEndDate: null,
          isDeleted: false,
          project,
          country,
          team,
          status,
        };
      }
    });
  }

  private generateTeams() {
    return this.teamsMockData.map((eachTeam, index) => {
      return { ...eachTeam };
    });
  }

  private generateProjects() {
    return this.projectsMockData.map((eachProject, index) => {
      const country =
        ((index + this._numberOfCountries) % this._numberOfCountries) + 1;

      return { ...eachProject, country };
    });
  }
}
