import { MOCK_PROJECT_DATA } from './../assets/MOCK_PROJECT_DATA';
import { DataProvider } from './data.provider';
import { MOCK_USER_DATA } from '../assets/MOCK_USER_DATA';
import { MOCK_TEAM_DATA } from '../assets/MOCK_TEAM_DATA';
import { CoreProvider } from './core.provider';
import { createConnection } from 'typeorm';

// SENSITIVE DATA ALERT! - Normally the seed and the admin credentials should not be present in the public repository!
// Run: `npm run seed` to seed the database

const execute = async () => {
  console.log('Seed started!');

  const connection = await createConnection();

  const dataProvider = new DataProvider(
    MOCK_USER_DATA,
    MOCK_TEAM_DATA,
    MOCK_PROJECT_DATA,
  );

  const seed = new CoreProvider(connection, dataProvider);

  try {
    await seed.beginSetup();

    await seed.seedProjects();
    await seed.seedTeams();
    await seed.seedUsers();

    await seed.completeSetup();
  } catch (e) {
    console.log(e.message);
  }

  console.log('Seed completed!');
  connection.close();
};

execute().catch(console.error);
