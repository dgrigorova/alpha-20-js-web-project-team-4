import { Project } from './../src/project/model/project.entity';
import { DbSetup } from '../src/common/model/dbsetup.entity';
import { User } from './../src/users/model/user.entity';
import { Team } from './../src/team/model/team.entity';
import { DataProvider } from './data.provider';
import { Repository } from 'typeorm';

export class CoreProvider {
  constructor(
    private readonly _connection: any,
    private readonly dataProvider: DataProvider,
  ) {}

  private async users() {
    return await this.dataProvider.users();
  }
  private teams() {
    return this.dataProvider.teams();
  }

  private projects() {
    return this.dataProvider.projects();
  }
  private async getRepository<T>(entity): Promise<Repository<T>> {
    return await this._connection.manager.getRepository(entity);
  }

  async seedUsers() {
    for (const user of await this.users()) {
      const userRepository = await this.getRepository<User>(User);

      const userToCreate = await userRepository.create(user);

      console.log(await userRepository.save(userToCreate));
    }
  }

  async seedProjects() {
    for (const project of this.projects()) {
      const projectRepository = await this.getRepository<Project>(Project);

      const projectsToCreate = await projectRepository.create(project);

      console.log(await projectRepository.save(projectsToCreate));
    }
  }
  async seedTeams() {
    for (const team of this.teams()) {
      const teamRepository = await this.getRepository<Team>(Team);

      const teamToCreate = await teamRepository.create(team);

      console.log(await teamRepository.save(teamToCreate));
    }
  }

  async beginSetup() {
    const dbSetupRepo = await this._connection.manager.getRepository(DbSetup);
    const setupData = await dbSetupRepo.find({});
    if (setupData.length) {
      throw new Error(`Data has been already set up`);
    }
  }

  async completeSetup() {
    const dbsetupRepo = await this._connection.manager.getRepository(DbSetup);
    await dbsetupRepo.save({ message: 'Setup has been completed!' });
  }
}
