'use strict';


customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">server documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/ApiModule.html" data-type="entity-link">ApiModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-ApiModule-393339e89dee09793638e4ae2eb608af"' : 'data-target="#xs-controllers-links-module-ApiModule-393339e89dee09793638e4ae2eb608af"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-ApiModule-393339e89dee09793638e4ae2eb608af"' :
                                            'id="xs-controllers-links-module-ApiModule-393339e89dee09793638e4ae2eb608af"' }>
                                            <li class="link">
                                                <a href="controllers/ApiController.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ApiController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-ApiModule-393339e89dee09793638e4ae2eb608af"' : 'data-target="#xs-injectables-links-module-ApiModule-393339e89dee09793638e4ae2eb608af"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-ApiModule-393339e89dee09793638e4ae2eb608af"' :
                                        'id="xs-injectables-links-module-ApiModule-393339e89dee09793638e4ae2eb608af"' }>
                                        <li class="link">
                                            <a href="injectables/ApiService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>ApiService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link">AppModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/AuthModule.html" data-type="entity-link">AuthModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-AuthModule-cccff23b67bed2f93754649cfebf6d35"' : 'data-target="#xs-controllers-links-module-AuthModule-cccff23b67bed2f93754649cfebf6d35"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-AuthModule-cccff23b67bed2f93754649cfebf6d35"' :
                                            'id="xs-controllers-links-module-AuthModule-cccff23b67bed2f93754649cfebf6d35"' }>
                                            <li class="link">
                                                <a href="controllers/AuthController.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AuthController</a>
                                            </li>
                                            <li class="link">
                                                <a href="controllers/UsersController.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">UsersController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-AuthModule-cccff23b67bed2f93754649cfebf6d35"' : 'data-target="#xs-injectables-links-module-AuthModule-cccff23b67bed2f93754649cfebf6d35"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-AuthModule-cccff23b67bed2f93754649cfebf6d35"' :
                                        'id="xs-injectables-links-module-AuthModule-cccff23b67bed2f93754649cfebf6d35"' }>
                                        <li class="link">
                                            <a href="injectables/AuthService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>AuthService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/JwtStrategy.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>JwtStrategy</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/CommonModule.html" data-type="entity-link">CommonModule</a>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-CommonModule-23d3d6691c9c7e58da216440e5455d6e"' : 'data-target="#xs-injectables-links-module-CommonModule-23d3d6691c9c7e58da216440e5455d6e"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-CommonModule-23d3d6691c9c7e58da216440e5455d6e"' :
                                        'id="xs-injectables-links-module-CommonModule-23d3d6691c9c7e58da216440e5455d6e"' }>
                                        <li class="link">
                                            <a href="injectables/FindService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>FindService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/TransformService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>TransformService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/CountryModule.html" data-type="entity-link">CountryModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-CountryModule-73f92375c2ed91faf9ab95bd13d7f5a7"' : 'data-target="#xs-controllers-links-module-CountryModule-73f92375c2ed91faf9ab95bd13d7f5a7"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-CountryModule-73f92375c2ed91faf9ab95bd13d7f5a7"' :
                                            'id="xs-controllers-links-module-CountryModule-73f92375c2ed91faf9ab95bd13d7f5a7"' }>
                                            <li class="link">
                                                <a href="controllers/CountryController.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">CountryController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-CountryModule-73f92375c2ed91faf9ab95bd13d7f5a7"' : 'data-target="#xs-injectables-links-module-CountryModule-73f92375c2ed91faf9ab95bd13d7f5a7"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-CountryModule-73f92375c2ed91faf9ab95bd13d7f5a7"' :
                                        'id="xs-injectables-links-module-CountryModule-73f92375c2ed91faf9ab95bd13d7f5a7"' }>
                                        <li class="link">
                                            <a href="injectables/CountryService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>CountryService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/HistoryModule.html" data-type="entity-link">HistoryModule</a>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-HistoryModule-742f538117e9ba1720c7bec39941258f"' : 'data-target="#xs-injectables-links-module-HistoryModule-742f538117e9ba1720c7bec39941258f"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-HistoryModule-742f538117e9ba1720c7bec39941258f"' :
                                        'id="xs-injectables-links-module-HistoryModule-742f538117e9ba1720c7bec39941258f"' }>
                                        <li class="link">
                                            <a href="injectables/HistoryService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>HistoryService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/PlanningModule.html" data-type="entity-link">PlanningModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-PlanningModule-c807f144bfecd7c927f943041341780a"' : 'data-target="#xs-controllers-links-module-PlanningModule-c807f144bfecd7c927f943041341780a"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-PlanningModule-c807f144bfecd7c927f943041341780a"' :
                                            'id="xs-controllers-links-module-PlanningModule-c807f144bfecd7c927f943041341780a"' }>
                                            <li class="link">
                                                <a href="controllers/PlanningController.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PlanningController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-PlanningModule-c807f144bfecd7c927f943041341780a"' : 'data-target="#xs-injectables-links-module-PlanningModule-c807f144bfecd7c927f943041341780a"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-PlanningModule-c807f144bfecd7c927f943041341780a"' :
                                        'id="xs-injectables-links-module-PlanningModule-c807f144bfecd7c927f943041341780a"' }>
                                        <li class="link">
                                            <a href="injectables/EmployeesPlanningService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>EmployeesPlanningService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/FloorService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>FloorService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/ProjectModule.html" data-type="entity-link">ProjectModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-ProjectModule-0ea226ae8498b7fdb82c82451f4db28a"' : 'data-target="#xs-controllers-links-module-ProjectModule-0ea226ae8498b7fdb82c82451f4db28a"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-ProjectModule-0ea226ae8498b7fdb82c82451f4db28a"' :
                                            'id="xs-controllers-links-module-ProjectModule-0ea226ae8498b7fdb82c82451f4db28a"' }>
                                            <li class="link">
                                                <a href="controllers/ProjectController.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ProjectController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-ProjectModule-0ea226ae8498b7fdb82c82451f4db28a"' : 'data-target="#xs-injectables-links-module-ProjectModule-0ea226ae8498b7fdb82c82451f4db28a"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-ProjectModule-0ea226ae8498b7fdb82c82451f4db28a"' :
                                        'id="xs-injectables-links-module-ProjectModule-0ea226ae8498b7fdb82c82451f4db28a"' }>
                                        <li class="link">
                                            <a href="injectables/ProjectService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>ProjectService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/TransformService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>TransformService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/UsersModule.html" data-type="entity-link">UsersModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-UsersModule-1ee490813518606107f2a26ed5233e7e"' : 'data-target="#xs-controllers-links-module-UsersModule-1ee490813518606107f2a26ed5233e7e"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-UsersModule-1ee490813518606107f2a26ed5233e7e"' :
                                            'id="xs-controllers-links-module-UsersModule-1ee490813518606107f2a26ed5233e7e"' }>
                                            <li class="link">
                                                <a href="controllers/UsersController.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">UsersController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-UsersModule-1ee490813518606107f2a26ed5233e7e"' : 'data-target="#xs-injectables-links-module-UsersModule-1ee490813518606107f2a26ed5233e7e"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-UsersModule-1ee490813518606107f2a26ed5233e7e"' :
                                        'id="xs-injectables-links-module-UsersModule-1ee490813518606107f2a26ed5233e7e"' }>
                                        <li class="link">
                                            <a href="injectables/UsersService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>UsersService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                </ul>
                </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#controllers-links"' :
                                'data-target="#xs-controllers-links"' }>
                                <span class="icon ion-md-swap"></span>
                                <span>Controllers</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="controllers-links"' : 'id="xs-controllers-links"' }>
                                <li class="link">
                                    <a href="controllers/ApiController.html" data-type="entity-link">ApiController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/AuthController.html" data-type="entity-link">AuthController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/CountryController.html" data-type="entity-link">CountryController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/PlanningController.html" data-type="entity-link">PlanningController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/ProjectController.html" data-type="entity-link">ProjectController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/UsersController.html" data-type="entity-link">UsersController</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/AdditionalFloorInfo.html" data-type="entity-link">AdditionalFloorInfo</a>
                            </li>
                            <li class="link">
                                <a href="classes/CoreProvider.html" data-type="entity-link">CoreProvider</a>
                            </li>
                            <li class="link">
                                <a href="classes/Country.html" data-type="entity-link">Country</a>
                            </li>
                            <li class="link">
                                <a href="classes/CreateFloorDTO.html" data-type="entity-link">CreateFloorDTO</a>
                            </li>
                            <li class="link">
                                <a href="classes/CreateProjectDTO.html" data-type="entity-link">CreateProjectDTO</a>
                            </li>
                            <li class="link">
                                <a href="classes/CreateUserDTO.html" data-type="entity-link">CreateUserDTO</a>
                            </li>
                            <li class="link">
                                <a href="classes/CreateVacationDTO.html" data-type="entity-link">CreateVacationDTO</a>
                            </li>
                            <li class="link">
                                <a href="classes/DataProvider.html" data-type="entity-link">DataProvider</a>
                            </li>
                            <li class="link">
                                <a href="classes/DbSetup.html" data-type="entity-link">DbSetup</a>
                            </li>
                            <li class="link">
                                <a href="classes/EditFloorDTO.html" data-type="entity-link">EditFloorDTO</a>
                            </li>
                            <li class="link">
                                <a href="classes/Floor.html" data-type="entity-link">Floor</a>
                            </li>
                            <li class="link">
                                <a href="classes/History.html" data-type="entity-link">History</a>
                            </li>
                            <li class="link">
                                <a href="classes/JwtPayload.html" data-type="entity-link">JwtPayload</a>
                            </li>
                            <li class="link">
                                <a href="classes/LoginUserDTO.html" data-type="entity-link">LoginUserDTO</a>
                            </li>
                            <li class="link">
                                <a href="classes/Project.html" data-type="entity-link">Project</a>
                            </li>
                            <li class="link">
                                <a href="classes/ResponseCountryDTO.html" data-type="entity-link">ResponseCountryDTO</a>
                            </li>
                            <li class="link">
                                <a href="classes/ResponseFloorDTO.html" data-type="entity-link">ResponseFloorDTO</a>
                            </li>
                            <li class="link">
                                <a href="classes/ResponseProjectDTO.html" data-type="entity-link">ResponseProjectDTO</a>
                            </li>
                            <li class="link">
                                <a href="classes/ResponseUserDTO.html" data-type="entity-link">ResponseUserDTO</a>
                            </li>
                            <li class="link">
                                <a href="classes/ResponseVacationDTO.html" data-type="entity-link">ResponseVacationDTO</a>
                            </li>
                            <li class="link">
                                <a href="classes/ServerSideFloor.html" data-type="entity-link">ServerSideFloor</a>
                            </li>
                            <li class="link">
                                <a href="classes/Team.html" data-type="entity-link">Team</a>
                            </li>
                            <li class="link">
                                <a href="classes/ThresholdDTO.html" data-type="entity-link">ThresholdDTO</a>
                            </li>
                            <li class="link">
                                <a href="classes/ThresholdValues.html" data-type="entity-link">ThresholdValues</a>
                            </li>
                            <li class="link">
                                <a href="classes/Token.html" data-type="entity-link">Token</a>
                            </li>
                            <li class="link">
                                <a href="classes/User.html" data-type="entity-link">User</a>
                            </li>
                            <li class="link">
                                <a href="classes/Vacation.html" data-type="entity-link">Vacation</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/ApiService.html" data-type="entity-link">ApiService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/AuthService.html" data-type="entity-link">AuthService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/CountryService.html" data-type="entity-link">CountryService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/EmployeesPlanningService.html" data-type="entity-link">EmployeesPlanningService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/FindService.html" data-type="entity-link">FindService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/FloorService.html" data-type="entity-link">FloorService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/HistoryService.html" data-type="entity-link">HistoryService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/JwtStrategy.html" data-type="entity-link">JwtStrategy</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ProjectService.html" data-type="entity-link">ProjectService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/TransformService.html" data-type="entity-link">TransformService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/UsersService.html" data-type="entity-link">UsersService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#guards-links"' :
                            'data-target="#xs-guards-links"' }>
                            <span class="icon ion-ios-lock"></span>
                            <span>Guards</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="guards-links"' : 'id="xs-guards-links"' }>
                            <li class="link">
                                <a href="guards/BlacklistGuard.html" data-type="entity-link">BlacklistGuard</a>
                            </li>
                            <li class="link">
                                <a href="guards/RolesGuard.html" data-type="entity-link">RolesGuard</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/enumerations.html" data-type="entity-link">Enums</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});