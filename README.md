<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg)" alt="logo" width="300px" style="margin-top: 20px;"/>

# COVID-19 Safe Workspace Portal - Telerik Academy Final Project Assignment - Jiraffe

## Description

A web portal that helps multinational companies with hot desk policy to ease the returning of their employees back to the office after COVID-19 lockdown.

Main consideration that must be taken into account – **average number of newly infected people for the last week in the particular country**.

Number of infected people per country is dynamically gathered periodically from https://github.com/javieraviles/covidAPI

Phased return to the office strategy should be applied.

## Functional Requirements:

<br>

*Public Part:*
- [x] Application MUST have public Homepage.
- [x] The portal MUST have view to display in a table current location of all the employees
(office/home/vacation) with their projects assigned.
- [x] The portal MUST have view to display in a table planned location of all the employees.
(office/home/vacation) with their projects assigned for the next week.
- [x] The portal MUST have view to display floor plan per country with the desks that are currently occupied, forbidden to be taken or available.
- [x] Application MUST have Register functionality - the User MUST fill in username, full name, country, e-mail, password and repeat password fields.
- [x] Application MUST have Login functionality - the User MUST be able to sign in with e-mail and password.
<br>

*Private Part:*
- [x] The user MUST be able to manage his planned vacations (enter new one and cancel/delete existing one).
- [x] Each user MUST be able to see his own data - when and where he is planned to work from.
- [x] Application MUST have logout functionality.
<br>

*Administration Part:*
- [x] Administrators MUST be able to manage company related data like Country, Number of desks, Projects (presented just like titles) and Participants in them.
- [x] Only users that are previously registered COULD be assigned to the projects.
- [x] Administrators SHOULD be able to configure and change the default numbers (threshold values) related to the phased return to the office strategy.
- [x] Administrators SHOULD be able to manage the floor planning.
- [x] Administrators MUST be able to press a button and gather data about the infected people per country on demand.


